<?php

use PHPUnit\TextUI\XmlConfiguration\CodeCoverage\Report\Html;

$db = \Config\Database::connect();
$query = $db->query('SELECT nama_indo, desc_indo, ket_indo FROM bahasa');
$results = $query->getRow();


return [
    'home' => 'Beranda',
    'product' => 'Produk',
    'company' => 'Tentang Kami',
    'contact' => 'Kontak',
    'gallery' => 'Galeri',
    'sisha' => 'Sisha/Hookah',
    'bbq' => 'Barbeque',
    'charcoal' => 'Charcoal',
    'vco' => 'VCO',
    'carbon' => 'Karbon Aktif',
    'copra' => 'Kopra',
    'presiden' => 'Presiden Direktur',
    'tentang' => 'Tentang Taiba',
    'mitra' => 'Mitra & Klien',
    'sertif' => 'Sertifikat & Penghargaan',



    'photo' => 'Foto',
    'video' => 'Video',
    'news' => 'Berita',
    'article' => 'Artikel',
    'newss' => 'Berita',



    'h1Title'                      => 'Selamat Datang',
    'lbhome'                      => 'Latar Belakang',
    'noteH1Title'                  => 'Kerangka kecil dengan fitur yang kuat',
    'sdhome1'                       => 'Indonesia adalah negara penghasil kelapa terbesar di dunia, dengan jumlah
    pesisir pantai terpanjang di dunia, menjadikan pohon kelapa menjadi
    komoditas andalan. Dengan area tanam 3,8 juta hektar membuat kelapa
    diolah menjadi berbagai jenis produk seperti : coco-chemical, fiber coco,
    virgin coconut oil, nata de coco, briket kelapa, asap cair dan banyak lainnya.',

    'sdhome2'                       => 'Briquittes atau briket menjadi salah satu andalan dari produk kelapa yang
    diolah dari tempurung kelapa, dibakar menjadi arang dengan metode
    tertentu untuk menghasilkan produk biomassa (energi terbarukan) dengan
    tingkat panas hingga 7000 kalori. Briket yg kami hasilkan 100% murni alami
    tanpa bahan kimia sama sekali, menjadi material yang aman sehingga tidak
    beracun ( non-toxic) dan tidak berasap. Briket arang tempurung kelapa yang
    kami hasilkan ketika dibakar akan menghasilkan asap yang sangat tipis
    bahkan hampir tidak berasap sama sekali sehingga aman untuk lingkungan.',

    'sdhome3'                       => 'Go Green Energy. Ini salah satu keunggulan briket arang tempurung kelapa
    yang paling terkenal. Go Green Energy ini adalah sebutan untuk sumber
    energi yang bersahabat bagi lingkungan hidup. Dengan keunggulan briket
    arang tempurung kelapa yang dibuat dari bahan baku kelapa dan juga
    bahan tambahan alami tepung tapioka, sehingga apabila terjadi
    pembakaran tidak akan menghasilkan asap, oleh karena itu briket arang
    tempurung kelapa ini sangat ramah terhadap lingkungan',

    'sdhome4' => 'Pengganti batu bara. Biasanya bahan bakar yang sering digunakan
    khususnya untuk industri itu adalah batu bara. Tapi seiring dengan
    perkembangan jaman, dan berdasarkan beberapa penelitian ternyata
    eksplorasi batu bara yang dilakukan secara masif akan sangat merusak
    alam. Selain itu, ternyata batu bara pun cenderung dan tergolong beracun
    apabila asapnya terhisap oleh manusia. Oleh karena itu briket arang
    tempurung kelapa yang kami produksi ini bisa menjadi solusi bahan bakar
    alternatif sebagai pengganti batu bara.',

    'sdhome5' => 'Waktu pembakaran yang lama. Briket arang tempurung kelapa ini memiliki
    ‘burning time’ atau waktu pembakaran yang lama, sekitar 2-3 jam non-stop.
    Menjadikannya briket ini sebagai bahan bakar yang tergolong efektif, aman
    dan efisien.',

    'lbcomj' => 'PT TAIBA COCOCHA INDONESIA',

    'lbcom' => 'PT. Taiba Cococha indonesia didirikan sejak januari 2020 yang merupakan
    group perusahaan PT. Blang Tamiang sejak tahun 2011. Pendiri dan tim
    produksi memiliki pengalaman dalam riset dan produksi sejak tahun 2011 di
    bawah BT group. Bahan baku yang berlimpah di berbagai wilayah kepulauan
    Indonesia menjadi potensi yang besar dalam pengolahan sumber daya alam.
    Kami juga berkomitmen melibatkan tenaga kerja dan SDM yang cukup besar
    dari lingkungan sekitar perusahaan, suplier bahan baku, transportasi antar
    pulau dan memperkerjakan jumlah pekerja yang sangat signifikan sehingga
    mampu mengurangi pengangguran. ',

    'lbcom2' => 'Kapasitas Pabrik briket kami mampu memproduksi rata - rata 10-15 ton
    perharinya. Dan beberapa negara yang menjadi target eksport briket
    diantaranya : Arab Saudi, Turki, Lebanon, Iraq, Jordan, Palestina, Syiria.
    Negara Eropa seperti Jerman, Inggris. Amerika dan Brazil. Negara Asia seperti
    Jepang dan beberapa negara lainnya. ',

    'lbcom3' => 'Selain mengekspor briket, kami juga menjual briket ke restoran - restoran
    yang dulunya menggunakan arang kayu atau arang batok kelapa, sekarang
    beralih ke briket. Diantara faktor peralihan bahan bakar tersebut dikarenakan
    penggunaan bahan bakar briket tidak berbau, api tidak memercik, tidak ada
    asap yang keluar seperti arang kayu, mudah dinyalakan, tahan hingga 3 jam
    lamanya, panas api yang tinggi hingga mencapai 7000 kalori. Kami juga
    memenuhi kebutuhan arang briket untuk konsumsi shisha/hookah karena
    aman dari bahan kimia dan tidak beracun.',

    'lb1' => 'Indonesia adalah salah satu produsen kelapa terbesar di dunia, dengan jumlah luas panjang pesisir pantai terpanjang di dunia,
    menjadikan pohon kelapa menjadi komoditas andalan, dengan area tanam 3,8 juta hektar membuat kelapa diolah menjadi berbagai jenis produk seperti : coco-chemical,
    fiber coco, minyak goreng, nata de coco, arang kelapa, bio mass dan lain sebagainya.',

    'lb2' => 'Briquettes atau briket menjadi salah satu andalan dari produk
    kelapa yang diolah dari tempurung kelapa yang dibakar hingga menjadi arang, dengan metode tertentu untuk menghasilkan produk dengan kadar air
    rendah dan daya ikat karbon tinggi.',

    'bchome' => 'Briket Kelapa',
    'bc1' => 'Proses mulai dari pemilihan kelapa, produksi arang hingga menjadi produk jadi.',
    'bc2' => 'Kami akan selalu menjamin mutu dan kualitas produk yang kami hasilkan dengan proses produksi yang termonitor.',

    'products' => 'PRODUK TAIBA',
    'productsd' => 'Produk Taiba didesign dengan metode pembuatan yang natural, dengan kelapa arang pilihan untuk menjamin mutu dan kualitas terbaik untuk hasil akhir dari produk briket coconut kami. Dengan metode tertentu untuk menghasilkan produk dengan kadar air rendah dan daya ikat karbon tinggi.
    Produk kami 100% natural, chemical free atau bebas kimia, dan sangat ecological friendly atau ramah lingkungan. Berikut adalah beberapa range produk dari kami.',
    'products1' => $results->nama_indo,
    'products2' => $results->desc_indo,
    'products3' => 'TAIBA COCO GOLD 72 PCS',
    'products4' => 'COCONUT CHARCOAL BRIQUETTES',
    'products5' => $results->ket_indo,


    'tk' => 'TENTANG KAMI',
'fauzan' => 'Fauzan Kamil',
'judulart' => 'Pengusaha Emas Hitam',
'isiart' => 'Sosoknya begitu menginspirasi. Berkat kegigihan dan keuletannya, arang tempurung kelapa bisa ia ubah menjadi "emas hitam". Hebatnya, pria yang juga pengusaha EO ini terjun langsung membersihkan tempurung kelapa. Mulai dari serabut-serabutnya untuk kemudian dijadikan arang. Ia sempat terjerembab ke dasar ngarai kegagalan, tapi semangatnya tak pernah padam. Berparas sederhana, murah senyum serta berjiwa sosial tinggi membuat pria asal Padang ini disenangi banyak orang khususnya kaum marjinal. Mulai dari anak-anak jalanan hingga pria pengangguran tak sungkan dirangkulnya. Dia adalah Hadi Kurnia Zaimi, petani arang tempurung kelapa dan pembina Usaha Kecil Menengah (UKM). " Jangan malu untuk memulai sesuatu dari bawah," ujar Hadi saat berbincang dengan Taibacoco.co.id di sela memberikan bimbingan kepada para petani arang tempurung kelapa di Pariaman Sumatera Barat , Rabu 21 April 2021. Berawal dari emas hitam inilah kehidupan Pria kelahiran Padang 30 Juni 1994 mengalir hingga ke Bogor Jawa Barat. Menyadari bukan berasal dari keluarga berada, Hadi gigih menekuni setiap pekerjaan yang dijalaninya. Kehidupan masa remajanya bisa dibilang penuh dengan perjuangan. Anak keempat pasangan H. Zainal Rj. marah (Alm) dan Hj. Emi suarni ini, sejak semester pertama kuliah sudah nyambi bekerja paruh waktu sebagai pengusaha EO . Tidak menyelesaikan kuliahnya karena semangat berwira usaha, Hadi kemudian menikah dengan pujaan hatinya Yolanda dan memiliki 1 orang putri bernama Haya kurnia yohadi . Sebagai keluarga muda yang perlu dukungan finansial, Hadi semakin bersemangat berwira usaha untuk mengumpulkan pundi pundi rupiah. Dengan dukungan Istri Tercinta dan ibundanya yang sangat menyayangi Hadi, diapun semakin terobsesi, maklum Hadi anak bungsu dan laki laki tunggal yang sangat disayagi Ibunda. " Aku selalu berusaha keras dan menikmati semua pekerjaan walaupun itu gagal. Kita tidak akan bangkit kalau tidak merasakan jatuh, belajarlah dari kegagalan dan bergurulah dari keberhasilan," kata pria pencinta olahraga sepakbola ini. Dengan bermodalkan sekitar 30 juta rupiah dari koceknya sendiri, Hadi melihat peluang di bisnis tempurung kelapa sangatlah cemerlang. Arang tempurung kelapa ternyata bisa di ekspor ke berbagai negara dan menjadi komoditas bisnis sangat tinggi di berbagai negara, karena digunakan sebagai bahan industri, farmasi dan lainnya. Meski prospeknya bagus, namun Hadi mengaku mengalami banyak kendala untuk memproduksi arang tempurung kelapa. Salah satunya, bahan baku tempurung kelapa masih terpencar-pencar di pasar-pasar tradisional. " Modal awal usaha sendiri, bisnis itu tidak selamanya berjalan mulus. Untuk sekarang ini saja terlalu sulit mencari bahan baku. Bisnisku bisa dibilang belum bisa berkembang besar," ujar pria asli minang ini. Saat memulai bisnisnya ini ia juga pernah diminta Bapedal Sumbar untuk menutup usaha pembuatan arang tempurung kelapa karena asapnya mencemari udara. Tetapi setelah Hadi menjelaskan usaha arang tempurung kelapa ini melibatkan banyak tenaga kerja dari kalangan orang-orang marjinal, Bapedal mengurungkan niatnya untuk menutup usahanya. Hingga saat ini Hadi masih memproduksi arang tempurung kelapa untuk mensuplay ke PT.Taiba cococha Indonesia yang memiliki kontrak ekspor ke berbagai negara. Selain arang tempurung kelapa yang memiliki nilai ekonomis tinggi, ternyata pengolahan asapnya juga menghasilkan liquid smoke. Produk ini memiliki nilai ekonomis yang sangat tinggi dan banyak diperlukan di berbagai negara maju sebagai pengannti formalin, pengental karet, sebagai bahan pembunuh hama dan pengganti insektisida. Permintaan liquid smoke, dari Hongkong, Jepang, dan negara maju Asia lainnya. " Akhirnya ketemu juga solusinya. Produk Liquid Smoke ternyata banyak dibutuhkan di pasar dunia," ujar Pria humoris ini. Ke depannya, Hadi memiliki keinginan mensinergikan potensi arang tempurung kelapa yang ada di seluruh propinsi Indonesia. " Peluang usaha pembuatan arang tempurung kelapa ini berpotensi mengangkat kesejahteraan masyarakat pesisir yang biasanya berada di sentra-sentra penghasil kelapa dan sebagian besar masih miskin." Tutup Hadi, menginspirasi Para pengusaha muda meraup hasil dari emas hitam tempurung kelapa (Abu Sarah)',


    'sandi' => 'Bersama Menteri Pariwisata dan Ekonomi Kreatif Sandiaga S. Uno',
    'bkpm' => 'Bersama Kepala BKPM Republik Indonesia Bahlil Lahadalia',
    'dubesarab1' => 'Bersama Duta Besar Arab Saudi HE. Essam Abid AlThaqafi',
    'dubesarab2' => 'Bersama Duta Besar Arab Saudi H.E Mustofa Ibrahim Mubarak',
    'tkdd' => 'INTRODUKSI & SPIRIT CEO',
    'tkd' => 'Kami dari PT. Taiba Cococha Indonesia berkomitmen akan terus berusaha
    melayani mitra pelanggan kami secara total dan maksimal. Dengan
    sumber daya alam kelapa yang berkualitas, pabrik pengolahan yang
    terintegrasi dengan online system kami, sumber daya manusia yang
    profesional, ahli dibidang charcoal briquettes dan memahami proses kerja
    dengan baik, membuat kami optimis bahwa kami dapat memenuhi
    kebutuhan mitra, bersaing secara global. Kami akan terus berinovasi,
    bekerja dengan values, profesionalitas sesuai deadline dan terus
    memperbaiki diri untuk memberi kepuasaan kepada para pelanggan kami <br><br>
    -- Owner & CEO PT Taiba Cococha Indonesia',
    'vm' => 'VISI & MISI
    PT TAIBA
    COCOCHA
    INDONESIA',
    'vm1' => 'VISI ',
    'vm2' => '“Menjadi perusahaan produsen produk briket arang kelapa berkualitas dunia”',
    'vm3' => 'MISI',
    'vm4' => '• Menciptakan produk kelas dunia yang natural dan bersih berkualitas',
    'vm5' => '• Menjadikan perusahaan yang berprinsip “sustainable” dan bersama sama menciptakan suasana kerja yang penuh anugrah',
    'vm6' => 'Turut serta dalam mendorong pertumbuhan ekonomi di Indonesia dengan membantu sesama',

    'lbc1' => 'LATAR BELAKANG',
    'lbc2' => 'Indonesia adalah salah satu produsen kelapa terbesar di dunia, dengan jumlah luas panjang pesisir pantai terpanjang di dunia,
    menjadikan pohon kelapa menjadi komoditas andalan, dengan area tanam 3,8 juta hektar membuat kelapa diolah menjadi berbagai jenis produk seperti : coco-chemical,
    fiber coco, minyak goreng, nata de coco, arang kelapa, bio mass dan lain sebagainya.',
    'lbc3' => 'Briquettes atau briket menjadi salah satu andalan dari produk
    kelapa yang diolah dari tempurung kelapa yang dibakar hingga menjadi arang, dengan metode tertentu untuk menghasilkan produk dengan kadar air
    rendah dan daya ikat karbon tinggi.',

    'c1' => 'HEAD OFFICE',
    'c2' => 'PT TAIBA COCOCHA INDONESIA Jl. Angsana Raya blok O no 2 pejaten timur
    Pasar Minggu Jakarta Selatan 12510, Indonesia',
    'c3' => 'Phone : +6221- 22790271',
    'c4' => 'Email : contact@taibacoco.co.id',
    'c5' => 'YOUR NAME (REQUIRED)',
    'c6' => 'YOUR EMAIL (REQUIRED)',
    'c7' => 'SUBJECT',
    'c8' => 'YOUR MESSAGE',
    'c9' => 'SEND',


];

<?php

$db = \Config\Database::connect();
$query = $db->query('SELECT nama_inggris, desc_inggris, ket_inggris FROM bahasa');
$results = $query->getRow();

return [
    'home' => 'HOME',
    'product' => 'PRODUCTS',
    'company' => 'About Us',
    'contact' => 'CONTACT',
    'gallery' => 'GALLERY',
    'sisha' => 'Sisha/Hookah',
    'bbq' => 'Barbeque',
    'charcoal' => 'Charcoal',
    'vco' => 'VCO',
    'carbon' => 'Activated Carbon',
    'copra' => 'Copra',
    'presiden' => 'President Director',
    'tentang' => 'About Taiba',
    'mitra' => 'Partners & Client',
    'sertif' => 'Achievement & Certificate',

    'photo' => 'Photo',
    'video' => 'Video',
    'news' => 'NEWS',
    'article' => 'ARTICLE',
    'newss' => 'NEWS',

    'h1Title'                      => 'Welcome',
    'lbhome'                       => 'Background',
    'noteH1Title'                  => 'The small framework with powerful features',
    'sdhome1'                      => 'Indonesia has become one of the biggest coconut producer in the world,
    with the longest coastal line in the world, resulting the coconut trees as the
    mainstay comodity. With 3,8 Million acre of planting area, coconut then
    processed into various product such as : Coco-chemical, Fibre Coco, Virgin
    Coconut Oil, Nata de Coco, Coconut Briquette, Liquid smoke and others.',

    'sdhome2'                       => 'Briquittes are one of the mainstays of coconut products that are processed
    from coconut shells,then burnt to become charcoal with specific method to
    produce biomass (sustainable energy) with heat level up to 7000 calori. Our
    produced briquettes is pure 100% natural without chemical ingredient,
    creating a safe, non-toxic, smokeless material. When burnt, our briquette
    charcoal will produce very thin layer of smoke to smokeless, so it will be safe
    for the environment.',

    'sdhome3'                       => 'Go Green Energy. This is one of the most famous advantages of coconut
    shell charcoal briquettes. Go Green Energy is the term for an energy source
    that is friendly to the environment. With the advantages of coconut shell
    charcoal briquettes that made from coconut material and also natural
    additives of tapioca flour, so that its combustion wont produce smoke (or
    less smoke), therefore coconut shell charcoal briquettes are very friendly to
    the environment.',

    'sdhome4' => 'Coal substitute. Usually coal is often used especially for the industry. But
    along with times, and based on several studies, it turns out that massive coal
    exploration will cause more damage to nature. In addition, it turns out that
    coal tends to be classified as poisonous when the smoke is inhaled by
    humans. Therefore, the coconut shell charcoal briquettes that we produce
    can be an alternative fuel solution as a substitute for coal.',

    'sdhome5' => 'Longer burning time. Coconut shell briquette charcoal has long burning time,
    up to 2-3 hours non-stop. Making this briquette as a fuel that is classified as
    effective, safe and efficient.
    ',

    'lbcomj' => 'PT TAIBA COCOCHA INDONESIA',

    'lbcom' => 'PT. Taiba Cococha indonesia is established on January 2020, part of PT BLANG
    TAMIANG group companies that established on 2011. The owner and
    production team has experienced on research and production since 2011
    under BT Group. The abundant raw materials in various parts of the
    Indonesian archipelago are great potential for processing natural resources.
    We are also committed on involving a sizeable workforce and human
    resources from the environment around the company, raw materials supplier,
    inter-island transportation and employ a very significant number of workers to
    reduce unemployment.
     ',

    'lbcom2' => 'Capacity Our briquette factory is able to produce an average of 10-15 tons per
    day. And several countries that become the target of our briquette exports
    include : Saudi Arabia, Turkey, Lebanon, Iraq, Jordan, Palestine, Syiria.
    European countries such as : Germany, Sweden and England. United States of
    America and Brazil. Asian countries such as : Japan and other several other
    countries.',

    'lbcom3' => 'Apart from exporting briquettes, we also provide briquette to restaurants that
    previously used wood or coconut charcoal then switch to briquettes. The
    switching factor from traditional charcoal to briquette are due to : briquettes
    is odorless, less spark, smokeless compared to wooden charcoal, easy to
    ignite, longer burning time (3 hours), glowing high heat of fire up to 7000
    calori. We also fullfil the needs of charcoal briquettes for shisha / hookah
    consumption because it is safe from chemicals and non-toxic.',

    'lb1' => 'Indonesia is one of the largest coconut producers in the world, with the longest long coastline in the world,
    making coconut trees a mainstay commodity, with a planted area of 3.8 million hectares making coconuts processed into various types of products such as: coco-chemical,
    fiber coco, cooking oil, nata de coco, coconut charcoal, bio mass and so on.',

    'lb2' => 'Briquettes or briquettes are one of the mainstays of the product
    coconut which is processed from coconut shell which is burned to become charcoal, with a certain method to produce a product with a moisture content
    low and high carbon binding capacity.',

    'bchome' => 'Briquette Charcoal',
    'bc1' => 'The process starts from coconut selection, charcoal production to finished products.',
    'bc2' => 'We will always guarantee the quality and quality of the products we produce with a monitored production process.',

    'products' => 'TAIBA PRODUCT',
    'productsd' => 'Taiba products are designed with natural manufacturing methods, with selected coconut charcoal to ensure the best quality and quality for the final product of our coconut briquettes. With certain methods to produce products with low moisture content and high carbon binding capacity.
    Our products are 100% natural, chemical free, and very ecological friendly. Here are some of our product ranges.',
    'products1' => $results->nama_inggris,
    'products2' => $results->desc_inggris,
    'products3' => 'TAIBA COCO GOLD 72 PCS',
    'products4' => 'COCONUT CHARCOAL BRIQUETTES',
    'products5' => $results->ket_inggris,

    'tk' => 'ABOUT US',
'fauzan' => 'Fauzan Kamil',
'judulart' => 'Black Gold Entrepreneur',
'isiart' => 'His figure is so inspiring. Thanks to his tenacity and tenacity, he can turn coconut shell charcoal into "black gold". Remarkably, this man who is also an EO entrepreneur jumps in to clean coconut shells. Starting from the fibers to be turned into charcoal. He had fallen to the bottom of the canyon of failure, but his enthusiasm was never extinguished. A simple face, smiling and high social spirit, this man from Padang is loved by many people, especially the marginalized. Starting from street children to unemployed men he did not hesitate to embrace. He is Hadi Kurnia Zaimi, coconut shell charcoal farmer and coach of Small and Medium Enterprises (UKM). "Dont be ashamed to start from the bottom," said Hadi when talking with Taibacoco.co.id while providing guidance to coconut shell charcoal farmers in Pariaman, West Sumatra, Wednesday, April 21, 2021. Starting from black gold, this is the life of a man born in Padang 30. June 1994 flowed to Bogor, West Java. Realizing that he does not come from a wealthy family, Hadi is persistent in every job he does. His teenage life was arguably full of struggles. The fourth child of the couple H. Zainal Rj. angry (late) and Hj. Emi Suarni, since the first semester of college, he has worked part-time as an EO entrepreneur. Not finishing college because of his entrepreneurial spirit, Hadi then married his idol Yolanda and had 1 daughter named Haya Kurnia Yohadi. As a young family who needs financial support, Hadi is increasingly enthusiastic about doing business to collect rupiah coffers. With the support of his beloved wife and mother who loved Hadi so much, he became increasingly obsessed with it, knowing that Hadi is the youngest child and the only man whom Mother loves. "I always try hard and enjoy all the work even if it fails. We will not get up if we dont feel the fall, learn from failure and be jealous of success," said the man who loves soccer. With around 30 million rupiah from his own pocket, Hadi saw that the opportunities in the coconut shell business were brilliant. Coconut shell charcoal can actually be exported to various countries and is a very high business commodity in various countries, because it is used as an industrial, pharmaceutical and other material. Even though the prospect is good, Hadi admits that he has experienced many obstacles in producing coconut shell charcoal. One of them, the raw material for coconut shells is still scattered in traditional markets. "The initial capital for my own business, the business does not always run smoothly. For now, it is too difficult to find raw materials. I can say that my business has not been able to grow very large," said the man from Minang. When starting his business, he was also asked by the West Sumatra Bapedal to close the coconut shell charcoal making business because the smoke polluted the air. But after Hadi explained that the coconut shell charcoal business involved a lot of workers from marginalized people, Bapedal canceled his intention to close down the business. Until now, Hadi is still producing coconut shell charcoal to supply PT.Taiba Cococha Indonesia, which has export contracts to various countries. In addition to coconut shell charcoal which has high economic value, it turns out that the smoke processing also produces liquid smoke. This product has a very high economic value and is widely needed in various developed countries as a formalin repellent, rubber thickener, as a pest killer and a substitute for insecticides. Demand for liquid smoke, from Hong Kong, Japan and other Asian developed countries. "Finally we found a solution. Liquid Smoke products are in great demand in the world market," said this humorous man. In the future, Hadi has the desire to synergize the potential of coconut shell charcoal in all Indonesian provinces. "The business opportunity for making coconut shell charcoal has the potential to raise the welfare of coastal communities who are usually located in coconut producing centers and most of them are still poor." Close Hadi, inspiring young entrepreneurs to make money from black gold coconut shells (Abu Sarah)',
    'sandi' => 'Together with the Minister of Tourism and Creative Economy Sandiaga S. Uno',
    'bkpm' => 'Together with the Head of BKPM of the Republic of Indonesia Bahlil Lahadalia',
    'dubesarab1' => 'With the Ambassador of Saudi Arabia HE. Essam Abid AlThaqafi',
    'dubesarab2' => 'Together with the Ambassador of Saudi Arabia H.E Mustofa Ibrahim Mubarak',
    'tkdd' => 'INTRODUCTION AND CEO’S SPIRIT',
    'tkd' => 'PT Taiba Cococha Indonesia are commited to strive by serving our
    customer with maximum effort and total dedication. With our quality
    coconut resources, production factory that integrated with our online
    system, professional human resources staff, experts in the field of
    charcoal briquettes which well understand the production process. Those
    factors makes us optimistic that we can fulfill the needs and demand of
    our customers, and compete globally. We will always continue to
    innovate, working with our core values, working professionaly to meet the
    deadline, and keep developing ourselves for our customers satisfaction.<br><br>
    -- Owner & CEO PT Taiba Cococha Indonesia',
    'vm' => 'VISION AND MISSION
    PT TAIBA
    COCOCHA
    INDONESIA',
    'vm1' => 'VISION',
    'vm2' => '"To become a world-class coconut charcoal briquette producer company"',
    'vm3' => 'MISSION',
    'vm4' => '• Creating world-class products that are natural and of clean quality',
    'vm5' => '• make a company with the principle of "sustainable" and together create a work atmosphere that is full of grace',
    'vm6' => 'Participate in encouraging economic growth in Indonesia by helping others',

    'lbc1' => 'BACKGROUND',
    'lbc2' => 'Indonesia is one of the largest coconut producers in the world, with the longest long coastline in the world,
    making coconut trees a mainstay commodity, with a planted area of 3.8 million hectares making coconuts processed into various types of products such as: coco-chemical,
    fiber coco, cooking oil, nata de coco, coconut charcoal, bio mass and so on.',
    'lbc3' => 'Briquettes or briquettes are one of the mainstays of the product
    coconut which is processed from coconut shell which is burned to become charcoal, with a certain method to produce a product with a moisture content
    low and high carbon binding capacity.',

    'c1' => 'HEAD OFFICE',
    'c2' => 'PT TAIBA COCOCHA INDONESIA Jl. Angsana Raya blok O no 2 pejaten timur
    Pasar Minggu Jakarta Selatan 12510, Indonesia',
    'c3' => 'Phone : +6221- 22790271',
    'c4' => 'Email : contact@taibacoco.co.id',
    'c5' => 'YOUR NAME (REQUIRED)',
    'c6' => 'YOUR EMAIL (REQUIRED)',
    'c7' => 'SUBJECT',
    'c8' => 'YOUR MESSAGE',
    'c9' => 'SEND',

];

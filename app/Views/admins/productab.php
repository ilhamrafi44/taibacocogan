<?= $this->extend('template/adminlayout'); ?>
<?= $this->section('admins'); ?>
<div class="main-panel">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Unggah Packaging (Indonesia)</h4>
                        <p class="card-description">
                            Form Upload Produk BBQ Untuk Bahasa Indonesia.
                        </p>
                        <form class="forms-sample" method="POST" action="<?= base_url(); ?>/admins/savepb" enctype="multipart/form-data">
                            <?= csrf_field(); ?>
                            <div class="form-group">
                                <label for="">Kategori Produk</label>
                                <select class="form-control" name="category_product" id="category_product" required>
                                    <option>Pilih Kategori</option>
                                    <option value="sisha">SISAH / HOOKAH</option>
                                    <option value="charcoal">CHARCOAL</option>
                                    <option value="bbq">BBQ</option>
                                    <option value="vco">VCO</option>
                                    <option value="carbon">ACTIVATED CARBON</option>
                                    <option value="copra">COPRA</option>

                                </select>
                            </div>
                            <div class="form-group">
                                <label for="nama_indo">Nama Produk</label>
                                <input type="text" class="form-control" name="nama_indo" id="nama_indo" aria-describedby="helpId" placeholder="" required>
                            </div>

                            <div class="form-group">
                                <label for="desc_indo">Deskripsi Produk</label>
                                <input type="text" class="form-control" name="desc_indo" id="desc_indo" aria-describedby="helpId" placeholder="">
                            </div>

                            <div class="form-group">
                                <label for="ket_indo">Keterangan Produk</label>
                                <input type="text" class="form-control" name="ket_indo" id="ket_indo" aria-describedby="helpId" placeholder="">
                            </div>

                            <div class="form-group">
                                <label>Unggah Gambar</label>
                                <input type="file" name="pict" class="form-control" required>
                            </div>

                    </div>
                </div>
            </div>

            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Upload Product (English)</h4>
                        <p class="card-description">
                            Product Form Upload For English Version.
                        </p>


                        <div class="form-group">
                            <label for="nama_inggris">Product Name</label>
                            <input type="text" class="form-control" name="nama_inggris" id="nama_inggris" aria-describedby="helpId" placeholder="" required>
                        </div>

                        <div class="form-group">
                            <label for="desc_inggris">Product Description</label>
                            <input type="text" class="form-control" name="desc_inggris" id="desc_inggris" aria-describedby="helpId" placeholder="">
                        </div>

                        <div class="form-group">
                            <label for="ket_inggris">Product Notes</label>
                            <input type="text" class="form-control" name="ket_inggris" id="ket_inggris" aria-describedby="helpId" placeholder="">
                        </div>



                    </div>
                </div>
            </div>

            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">تحميل المنتج (عربي)</h4>
                        <p class="card-description">
                            نموذج تحميل المنتج للغة العربية.
                        </p>


                        <div class="form-group">
                            <label for="nama_arab">اسم المنتج</label>
                            <input type="text" class="form-control" name="nama_arab" id="nama_arab" aria-describedby="helpId" placeholder="" required>
                        </div>

                        <div class="form-group">
                            <label for="desc_arab">وصف المنتج</label>
                            <input type="text" class="form-control" name="desc_arab" id="desc_arab" aria-describedby="helpId" placeholder="">
                        </div>

                        <div class="form-group">
                            <label for="ket_arab">وصف المنتج</label>
                            <input type="text" class="form-control" name="ket_arab" id="ket_arab" aria-describedby="helpId" placeholder="">
                        </div>



                        <button type="submit" class="btn btn-primary mr-2">Submit</button>
                        <button class="btn btn-light">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Upload Sisha Product Page Image Header</h4>
                        <p class="card-description">
                            Pastikan file berbentuk JPG,PNG,JPEG dan berukuran kurang dari 5MB.
                        </p>
                        <form class="forms-sample" method="post" action="<?= base_url(); ?>/admins/save" enctype="multipart/form-data">
                            <?= csrf_field(); ?>
                            <div class="form-group">
                                <label>File upload</label>
                                <input type="file" name="pict" class="form-control">
                            </div>
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button class="btn btn-light">Cancel</button>
                        </form>
                    </div>
                </div>
            </div> -->

            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">File yang sudah di upload</h4>
                        <div class="row">
                            <?php foreach ($gambar as $p) : ?>
                                <div class="col-md-4">
                                    <div class="row p-4">
                                        <div class="col-md-6">
                                            <img style="width:100%;" src="<?= base_url() . "/public/admins/uploads/" . $p['pict']; ?>" alt="">
                                        </div>
                                        <div class="col-md-6">
                                            <p class="text-warning" style="font-weight:bold;"><?= $p['nama_indo']; ?></p><br><br>
                                            <form enctype="multipart/form-data" action="<?= base_url() . "/admins/deletes/" . $p['id']; ?>" method="POST" style="display: inline;">
                                                <?= csrf_field(); ?>
                                                <button type="submit" class="btn btn-danger" onclick="return confirm('serina lu gan?');">Delete</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- content-wrapper ends -->
    <!-- partial:partials/_footer.html -->
    <footer class="footer">
        <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2021. <a href="<?php base_url(); ?>" target="_blank">Taiba Admin</a>.
                All rights reserved.</span>
        </div>
    </footer>
    <!-- partial -->
</div>
<?= $this->endSection(); ?>
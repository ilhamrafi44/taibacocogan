<?= $this->extend('template/adminlayout'); ?>
<?= $this->section('admins'); ?>
<div class="main-panel">
  <div class="content-wrapper">

    <div class="row">
      <div class="col-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Upload Image (Gambar 1)</h4>
            <p class="card-description">
              Pastikan file berbentuk JPG,PNG,JPEG dan berukuran kurang dari 5MB.
            </p>
            <form class="forms-sample" method="POST" action="<?= base_url(); ?>/admins/savearticle" enctype="multipart/form-data">
                <?= csrf_field(); ?>
              <div class="form-group">
                <label>File upload</label>
                <input type="file" name="pict_1" class="form-control">
              </div>

          </div>
        </div>
      </div>
      
      <div class="col-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Upload Image (Gambar 2)</h4>
            <p class="card-description">
              Pastikan file berbentuk JPG,PNG,JPEG dan berukuran kurang dari 5MB.
            </p>
         
              <div class="form-group">
                <label>File upload</label>
                <input type="file" name="pict_2" class="form-control">
              </div>

          </div>
        </div>
      </div>

      <div class="col-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
        
      

                
            <div class="form-group">
                <label for="nama_article_id">Masukan Nama Article (Indonesia)</label>
                    <input type="text" class="form-control" name="nama_article_id" id="nama_article_id" aria-describedby="helpId" placeholder="">
            </div>

            <div class="form-group">
                <label for="nama_article_en">Masukan Nama Article (Inggris)</label>
                    <input type="text" class="form-control" name="nama_article_en" id="nama_article_en" aria-describedby="helpId" placeholder="">
            </div>

            <div class="form-group">
                <label for="nama_article_ar">Masukan Nama Article (Arab)</label>
                    <input type="text" class="form-control" name="nama_article_ar" id="nama_article_ar" aria-describedby="helpId" placeholder="">
            </div>

            <div class="form-group">
              <label for="desc_article_id">Masukan Dekripsi Article (Indonesia)</label>
              <textarea class="form-control" name="desc_article_id" id="desc_article_id" rows="3"></textarea>
            </div>

            <div class="form-group">
                <label for="desc_article_en">Masukan Dekripsi Article (Inggris)</label>
                <textarea class="form-control" name="desc_article_en" id="desc_article_en" rows="3"></textarea>
              </div>

              <div class="form-group">
                <label for="desc_article_ar">Masukan Dekripsi Article (Arab)</label>
                <textarea class="form-control" name="desc_article_ar" id="desc_article_ar" rows="3"></textarea>
              </div>

              
              <button type="submit" class="btn btn-primary mr-2">Submit</button>
              <button class="btn btn-light">Cancel</button>
              
     
            
            </form>

          </div>
        </div>
      </div>



      <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">File yang sudah di upload</h4>
            <div class="row">
              <?php foreach ($gambar as $p) : ?>
                <div class="col">
                  <img style="width:150px;" src="<?= base_url() . "/public/admins/uploads/" . $p['pict_1']; ?>" alt="">
                  <form enctype="multipart/form-data" action="<?= base_url() . "/admins/delete/" . $p['id']; ?>" method="POST" style="display: inline;">
                    <?= csrf_field(); ?>
                    <button type="submit" class="btn btn-danger" onclick="return confirm('serina lu gan?');">Delete</button>
                  </form>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- content-wrapper ends -->
  <!-- partial:partials/_footer.html -->
  <footer class="footer">
    <div class="d-sm-flex justify-content-center justify-content-sm-between">
      <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2021. <a href="<?php base_url(); ?>" target="_blank">Taiba Admin</a>.
        All rights reserved.</span>
    </div>
  </footer>
  <!-- partial -->
</div>
<?= $this->endSection(); ?>
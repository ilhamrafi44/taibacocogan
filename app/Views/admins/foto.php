<?= $this->extend('template/adminlayout'); ?>
<?= $this->section('admins'); ?>
<div class="main-panel">
    <div class="content-wrapper">
        <form class="forms-sample" method="post" action="<?= base_url(); ?>/admins/savefoto" enctype="multipart/form-data">
            <?= csrf_field(); ?>
            <div class="row">
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Deskripsi (Indonesia)</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="desc_indo" id="desc_indo" aria-describedby="helpId" placeholder="">
                                <small id="helpId" class="form-text text-muted">Help text</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Description (English)</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="desc_inggris" id="desc_inggris" aria-describedby="helpId" placeholder="">
                                <small id="helpId" class="form-text text-muted">Help text</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">الوصف (عربي)</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="desc_arab" id="desc_arab" aria-describedby="helpId" placeholder="">
                                <small id="helpId" class="form-text text-muted">Help text</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Upload Foto</h4>
                            <p class="card-description">
                                Pastikan file berbentuk JPG,PNG,JPEG dan berukuran kurang dari 5MB.
                            </p>

                            <div class="form-group">
                                <label>File upload</label>
                                <input type="file" name="pict" class="form-control">
                            </div>
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button class="btn btn-light">Cancel</button>
                        </div>
                    </div>
                </div>
        </form>




        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">File yang sudah di upload</h4>
                    <div class="row">
                        <?php foreach ($gambar as $p) : ?>
                            <div class="col">
                                <img style="width:150px;" src="<?= base_url() . "/public/admins/uploads/" . $p['pict']; ?>" alt="">
                                <form enctype="multipart/form-data" action="<?= base_url() . "/admins/deletefoto/" . $p['id']; ?>" method="POST" style="display: inline;">
                                    <?= csrf_field(); ?>
                                    <button type="submit" class="btn btn-danger" onclick="return confirm('Apakah Anda Yakin?');">Delete</button>
                                </form>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- content-wrapper ends -->
<!-- partial:partials/_footer.html -->
<footer class="footer">
    <div class="d-sm-flex justify-content-center justify-content-sm-between">
        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2021. <a href="<?php base_url(); ?>" target="_blank">Taiba Admin</a>.
            All rights reserved.</span>
    </div>
</footer>
<!-- partial -->
</div>
<?= $this->endSection(); ?>
<?= $this->extend('template/adminlayout'); ?>
<?= $this->section('admins'); ?>
<div class="main-panel">
    <div class="content-wrapper">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Incoming Mails</h4>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Subject</th>
                                <th>Messages</th>
                            </thead>
                            <?php foreach ($mail as $p) : ?>
                            <tbody>
                                    <td><?= $p['name']; ?></td>
                                    <td><?= $p['subject']; ?></td>
                                    <td><?= $p['email']; ?></td>
                                    <td><?= $p['message']; ?></td>
                                    <td>
                                        <form enctype="multipart/form-data" action="<?= base_url() . "/admins/deletem/" . $p['id']; ?>" method="POST" style="display: inline;">
                                            <?= csrf_field(); ?>
                                            <button type="submit" class="btn btn-danger" onclick="return confirm('Apakah Anda Yakin?');">Delete</button>
                                        </form>
                                    </td>
                            </tbody>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection(); ?>
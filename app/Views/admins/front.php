<?= $this->extend('template/adminlayout'); ?>
<?= $this->section('admins'); ?>
<div class="main-panel">
  <div class="content-wrapper">

    <div class="row">
      <div class="col-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Upload Front Page Image Slider (Indonesia)</h4>
            <p class="card-description">
              Pastikan file berbentuk JPG,PNG,JPEG dan berukuran kurang dari 5MB.
            </p>
            <form class="forms-sample" method="post" action="<?= base_url(); ?>/admins/save" enctype="multipart/form-data">
              <?= csrf_field(); ?>
              <div class="form-group">
                <label>File upload</label>
                <input type="file" name="pict_indo" class="form-control">
              </div>

          </div>
        </div>
      </div>

      <div class="col-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Upload Front Page Image Slider (Inggris)</h4>
            <p class="card-description">
              Pastikan file berbentuk JPG,PNG,JPEG dan berukuran kurang dari 5MB.
            </p>

            <div class="form-group">
              <label>File upload</label>
              <input type="file" name="pict_inggris" class="form-control">
            </div>

          </div>
        </div>
      </div>

      <div class="col-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Upload Front Page Image Slider (Arab)</h4>
            <p class="card-description">
              Pastikan file berbentuk JPG,PNG,JPEG dan berukuran kurang dari 5MB.
            </p>

            <div class="form-group">
              <label>File upload</label>
              <input type="file" name="pict_arab" class="form-control">
            </div>
            <button type="submit" class="btn btn-primary mr-2">Submit</button>
            <button class="btn btn-light">Cancel</button>
            </form>
          </div>
        </div>
      </div>

      <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">File yang sudah di upload</h4>
            <div class="row">
              <?php foreach ($gambar as $p) : ?>
                <div class="col">
                  <img style="width:150px;" src="<?= base_url() . "/public/admins/uploads/" . $p['pict_indo']; ?>" alt="">
                  <form enctype="multipart/form-data" action="<?= base_url() . "/admins/delete/" . $p['id']; ?>" method="POST" style="display: inline;">
                    <?= csrf_field(); ?>
                    <button type="submit" class="btn btn-danger" onclick="return confirm('serina lu gan?');">Delete</button>
                  </form>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- content-wrapper ends -->
  <!-- partial:partials/_footer.html -->
  <footer class="footer">
    <div class="d-sm-flex justify-content-center justify-content-sm-between">
      <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2021. <a href="<?php base_url(); ?>" target="_blank">Taiba Admin</a>.
        All rights reserved.</span>
    </div>
  </footer>
  <!-- partial -->
</div>
<?= $this->endSection(); ?>
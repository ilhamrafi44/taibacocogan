<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Taiba Admins</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="public/admins/vendors/feather/feather.css">
    <link rel="stylesheet" href="public/admins/vendors/ti-icons/css/themify-icons.css">
    <link rel="stylesheet" href="public/admins/vendors/css/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="public/admins/vendors/datatables.net-bs4/dataTables.bootstrap4.css">
    <link rel="stylesheet" href="public/admins/vendors/ti-icons/css/themify-icons.css">
    <link rel="stylesheet" type="text/css" href="public/admins/js/select.dataTables.min.css">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="public/admins/css/vertical-layout-light/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="public/assets/img/logo.png" />
</head>

<body>
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="content-wrapper d-flex align-items-center auth px-0">
                <div class="row w-100 mx-0">
                    <div class="col-lg-4 mx-auto">
                        <div class="auth-form-light text-left py-5 px-4 px-sm-5">
                            <div class="brand-logo text-center">
                                <img style="width: 100px;" src="public/assets/img/logo.png" alt="logo">
                                <h2 class="text-center">Admin Page</h2>
                            </div>

                            <?php if (session()->getFlashdata('msg')) : ?>
                                <div class="alert alert-danger"><?= session()->getFlashdata('msg') ?></div>
                            <?php endif; ?>
                            <form action="<?= base_url(); ?>/home/auth" method="post">
                                <div class="form-group">
                                    <input name="email" type="email" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="Username">
                                </div>
                                <div class="form-group">
                                    <input name="password" type="password" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Password">
                                </div>
                                <div class="mt-3">
                                    <button type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">SIGN IN</button>
                                </div>
                                <div class="my-2 d-flex justify-content-between align-items-center">
                                    <div class="form-check">
                                        <label class="form-check-label text-muted">
                                            <input type="checkbox" class="form-check-input">
                                            Keep me signed in
                                        </label>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- plugins:js -->
    <script src="public/admins/vendors/js/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="public/admins/vendors/chart.js/Chart.min.js"></script>
    <script src="public/admins/vendors/datatables.net/jquery.dataTables.js"></script>
    <script src="public/admins/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
    <script src="public/admins/js/dataTables.select.min.js"></script>

    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="public/admins/js/off-canvas.js"></script>
    <script src="public/admins/js/hoverable-collapse.js"></script>
    <script src="public/admins/js/template.js"></script>
    <script src="public/admins/js/settings.js"></script>
    <script src="public/admins/js/todolist.js"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <script src="public/admins/js/dashboard.js"></script>
    <script src="public/admins/js/Chart.roundedBarCharts.js"></script>
    <!-- End custom js for this page-->
</body>

</html>
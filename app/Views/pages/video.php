<?= $this->extend('template/layout'); ?>
<?= $this->section('content'); ?>
<style>
.dotted {
    border: 6px dotted #ffffff;
    border-style: none none dotted;
    color: #fff;
}
</style>
<section id="intro" style="height: auto;">

    <img src="public/assets/img/gallerybg.png" alt="" style="width: 100%;">
    <!-- <h1 style="font-weight: bolder;">COMPANY</h1> -->

</section><!-- #intro -->

<main id="main">

    <!--==========================
      Featured Services Section
    ============================-->

    <?php foreach ($gambar as $p) : ?>





    <?php endforeach; ?>


    <section id="featured-services">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-5 box text-center">
                    <h4 class="title text-right"><a href="">VIDEO</a></h4>
                    <style>
                    .dotted {
                        border: 6px dotted #ffffff;
                        border-style: none none dotted;
                        color: #fff;
                    }
                    </style>
                    <hr class='dotted' />

                </div>

                <div class="col-lg-5 box text-center">

                </div>

                <div class="col-lg-2 box text-center">
                </div>

            </div>
            <div class="container">
                <div class="row">
                    <?php foreach ($gambar as $p) : ?>
                    <div class="col-md-3">
                        <iframe style="width:100%;" src="https://www.youtube.com/embed/<?= $p['link']; ?>"
                            title="YouTube video player" frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write;  gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                        <p style="padding-top: 10px;" class="text-center">
                            <?php if (WEB_LANG == 'id') {
                                    echo lang($p['desc_indo']);
                                } elseif (WEB_LANG == 'en') {
                                    echo lang($p['desc_inggris']);
                                } else {
                                    echo lang($p['desc_arab']);
                                } ?>
                        </p>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section><!-- #featured-services -->





</main>

<?= $this->endSection(); ?>
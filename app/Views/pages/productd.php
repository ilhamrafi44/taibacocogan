<?= $this->extend('template/layout'); ?>
<?= $this->section('content'); ?>
<style>
    .dotted {
        border: 6px dotted #ffffff;
        border-style: none none dotted;
        color: #fff;
    }
</style>
<section id="intro" style="height: auto;">

    <img src="public/assets/img/banner_vco.jpg" alt="" style="width: 100%;">
    <!-- <h1 style="font-weight: bolder;">COMPANY</h1> -->

</section><!-- #intro -->

<main id="main">

    <!--==========================
      Featured Services Section
    ============================-->
    <section id="featured-services">
        <div class="container">
            <div class="row">

                <div class="col-lg-12 text-center">

                    <img src="public/assets/img/ .png" alt="" style="width: 100%;">

                </div>


            </div>
        </div>
    </section><!-- #featured-services -->

    <!--==========================
      Featured Services Section
    ============================-->
   <!-- #featured-services -->
    <?php foreach ($bahasah as $p) : ?>
        <section id="featured-services">
            <div class="container-fluid">
                <div class="row">

                         <div class="col-lg-4 box text-center">

                                <?php if (WEB_LANG == 'id') { ?>
                                 <h4 style="font-size:22px;" class="title text-justify"><a href="#<?= $p['id']; ?>" id="<?= $p['id']; ?>">   <?= $p['nama_indo']?></a></h4>
                             <?php   } elseif (WEB_LANG == 'en') { ?>
                                         <h4 style="font-size:22px;" class="title text-justify"><a href="#<?= $p['id']; ?>" id="<?= $p['id']; ?>">   <?= $p['nama_inggris']?></a></h4>
                              <?php  } else {?>
                                         <h4 style="font-size:22px;" class="title text-right"><a href="#<?= $p['id']; ?>" id="<?= $p['id']; ?>">   <?= $p['nama_arab']?></a></h4>
                              <?php  } ?>
                              
                              
                              <?php if (WEB_LANG == 'id') { ?>
                                  <p style="font-size:20px;" class="text-justify"><?= $p['desc_indo'] ?></p>
                             <?php   } elseif (WEB_LANG == 'en') { ?>
                                          <p style="font-size:20px;" class="text-justify"><?= $p['desc_inggris'] ?></p>
                              <?php  } else {?>
                                        <p style="font-size:20px;" class="text-right"><?= $p['desc_arab'] ?></p>
                              <?php  } ?>
                    
                  
                        

                        <br>
                
                             <?php if (WEB_LANG == 'id') { ?>
                                  <p style="font-size:20px;" class="text-justify"><?= $p['ket_indo'] ?></p>
                             <?php   } elseif (WEB_LANG == 'en') { ?>
                                          <p style="font-size:20px;" class="text-justify"><?= $p['ket_inggris'] ?></p>
                              <?php  } else {?>
                                        <p style="font-size:20px;" class="text-right"><?= $p['ket_arab'] ?></p>
                              <?php  } ?>

                    </div>

                    <div class="col-lg-4 box text-center">
                        <div>
                            <img style="background-image: linear-gradient(
0deg
, #000000 0%, #242323 50%, #000000 100%);
    border-radius: 20px;
    border: solid gold; width:100%;" src="<?= base_url() . "/public/admins/uploads/" . $p['pict']; ?>" alt="">
                        </div>
                    </div>
                    <div class="col-lg-4 box text-center">
                    </div>



                </div>
            </div>
        </section>
    <?php endforeach; ?>







</main>

<?= $this->endSection(); ?>
<?= $this->extend('template/layout'); ?>
<?= $this->section('content'); ?>
<style>
    .dotted {
        border: 6px dotted #ffffff;
        border-style: none none dotted;
        color: #fff;
    }
</style>
<section id="intro" style="height: auto;">

    <img src="public/assets/img/contactbg.png" alt="" style="width: 100%;">
    <!-- <h1 style="font-weight: bolder;">COMPANY</h1> -->

</section><!-- #intro -->

<main id="main">

    <!--==========================
      Featured Services Section
    ============================-->
    <section id="featured-services">
        <div class="container">
            <div class="row">

                <div class="col-lg-6 text-left">

                    <h3 class="title text-left" ><a href=""><?= lang('Global.c1'); ?></a></h3>
                    <p><?= lang('Global.c2'); ?></p>
                    <br>
                    <p><?= lang('Global.c3'); ?></p>
                    <br>
                    <p><?= lang('Global.c4'); ?></p>

                </div>

                <div class="col-lg-6 text-left">

                    <form action="<?= base_url(); ?>/Home/sendEmail" method="post" enctype="multipart/form-data">
                        <?= csrf_field(); ?>

                        <label for="name" style="color: white; font-weight:bold;"><?= lang('Global.c5'); ?></label>
                        <input style="background-color:black; color:white;" class="form-control" type="text" name="name" id="name" required><br>

                        <label for="email" style="color: white; font-weight:bold;"><?= lang('Global.c6'); ?></label>
                        <input style="background-color:black; color:white;" class="form-control" type="email" name="email" id="email" required><br>

                        <label for="subject" style="color: white; font-weight:bold;"><?= lang('Global.c7'); ?></label>
                        <input style="background-color:black; color:white;" class="form-control" type="text" name="subject" id="subject" required><br>

                        <label for="message" style="color: white; font-weight:bold;"><?= lang('Global.c8'); ?></label>
                        <textarea style="background-color:black; color:white;" rows="7" class="form-control" name="message" id="message" required></textarea><br>

                        <button class="btn" style="background-color:#bd9221; color:#000000; border-radius:1px; width:140px; font-weight:bold;" type="submit"><?= lang('Global.c9'); ?></button>

                    </form>

                </div>


            </div>
        </div>
    </section><!-- #featured-services -->

</main>

<?= $this->endSection(); ?>
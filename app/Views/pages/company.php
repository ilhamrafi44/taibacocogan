<?= $this->extend('template/layout'); ?>
<?= $this->section('content'); ?>
<style>
.dotted {
    border: 6px dotted #ffffff;
    border-style: none none dotted;
    color: #fff;
}

#featured-services .box-bg1 {
    background-image: linear-gradient(0deg, #000000 0%, #242323 50%, #000000 100%);
    border-radius: 20px;
    border: solid gold;
    padding: 10px 10px 10px 10px;

}

.mantep>a {
    color: white;
    padding-left: 200px;

}

.carousel {
    position: relative;
}

.carousel-item img {
    object-fit: cover;
}

#carousel-thumbs {
    background: rgba(255, 255, 255, .3);
    bottom: 0;
    left: 0;
    padding: 0 50px;
    right: 0;
}

#carousel-thumbs img {
    border: 5px solid transparent;
    cursor: pointer;
}

#carousel-thumbs img:hover {
    border-color: rgba(255, 255, 255, .3);
}

#carousel-thumbs .selected img {
    border-color: #fff;
}

.carousel-control-prev,
.carousel-control-next {
    width: 50px;
}

@media all and (max-width: 767px) {
    .carousel-container #carousel-thumbs img {
        border-width: 3px;
    }
}

@media all and (min-width: 576px) {
    .carousel-container #carousel-thumbs {
        position: absolute;
    }
}

@media all and (max-width: 576px) {
    .carousel-container #carousel-thumbs {
        background: #ccccce;
    }
}

@media (max-width: 768px) {
    .mantep>a {
        color: white;
        padding-left: 0px;

    }

    #intro .carousel-item {
        width: 100%;
        height: 30vh;
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
    }
}
</style>
<section id="intro" style="height: auto;">

    <img src="public/assets/img/bangpozan.jpg" alt="" style="width: 100%;">
    <!-- <h1 style="font-weight: bolder;">COMPANY</h1> -->

</section><!-- #intro -->

<main id="main">
    <h1 style="display: none">cara bakar arang</h1>
    <h1 style="display: none">charcoal briquettes</h1>
    <h1 style="display: none">supplier arang</h1>
    <h1 style="display: none">arang barbeque</h1>
    <h1 style="display: none">coconut charcoal briquettes</h1>
    <h1 style="display: none">coconut shell briquettes</h1>

    <!--==========================
      Featured Services Section
    ============================-->
    <!-- <section id="featured-services">
        <div class="container">
            <div class="row">

                <div class="col-lg-12 text-center">

                    <img src="public/assets/img/box5.png" alt="" style="width: 100%;">

                </div>


            </div>
        </div>
    </section>#featured-services -->

    <!--==========================
      Featured Services Section
    ============================-->
    <style>
    #featured-services p {
        font-size: 16px;
        color: #fff;
    }

    #about p {
        font-size: 16px;
        color: #fff;
    }
    </style>
    <?php if (WEB_LANG == 'ar') {
        echo '  <style>#featured-services p {font-size: 22px;

            color: #fff;
            }

            #about p {font-size: 22px;

                color: #fff;
                }</style>';
    } ?>

    <section id="featured-services">
        <div class="container">
            <div class="row">
                <!--
                <div class="col-lg-4 box text-center">
                    <div class="">
                        <img style="background-image: linear-gradient(
0deg
, #000000 0%, #242323 50%, #000000 100%);
    border-radius: 20px;
    border: solid gold; width:100%;" src="public/assets/img/1.png" alt="">
                    </div>
                </div> -->

            </div>
        </div>
    </section>
    <h1 style="display: none">cara bakar arang</h1>
    <h1 style="display: none">charcoal briquettes</h1>
    <h1 style="display: none">supplier arang</h1>
    <h1 style="display: none">arang barbeque</h1>
    <h1 style="display: none">coconut charcoal briquettes</h1>
    <h1 style="display: none">coconut shell briquettes</h1>
    <section id="featured-services">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-5 box text-center">
                    <div class="d-flex justify-content-end">
                        <div class="container">
                            <div class="carousel-container position-relative row">

                                <!-- Sorry! Lightbox doesn't work - yet. -->

                                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">

                                        <div class="carousel-item active" data-slide-number="0">
                                            <img src="public/assets/img/bangpojan/1.jpeg" class="d-block w-100"
                                                alt="..." data-remote="public/assets/img/bangpojan/1.jpeg"
                                                data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
                                        </div>
                                        <div class="carousel-item" data-slide-number="1">
                                            <img src="public/assets/img/bangpojan/2.jpeg" class="d-block w-100"
                                                alt="..." data-remote="public/assets/img/bangpojan/2.jpeg"
                                                data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
                                        </div>
                                        <div class="carousel-item" data-slide-number="2">
                                            <img src="public/assets/img/bangpojan/3.jpeg" class="d-block w-100"
                                                alt="..." data-remote="public/assets/img/bangpojan/3.jpeg"
                                                data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
                                        </div>
                                        <div class="carousel-item" data-slide-number="3">
                                            <img src="public/assets/img/bangpojan/4.jpeg" class="d-block w-100"
                                                alt="..." data-remote="public/assets/img/bangpojan/4.jpeg"
                                                data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
                                        </div>
                                        <div class="carousel-item" data-slide-number="4">
                                            <img src="public/assets/img/bangpojan/6.jpg" class="d-block w-100" alt="..."
                                                data-remote="public/assets/img/bangpojan/6.jpg" data-type="image"
                                                data-toggle="lightbox" data-gallery="example-gallery">
                                        </div>
                                        <div class="carousel-item" data-slide-number="5">
                                            <img src="public/assets/img/bangpojan/7.jpg" class="d-block w-100" alt="..."
                                                data-remote="public/assets/img/bangpojan/7.jpg" data-type="image"
                                                data-toggle="lightbox" data-gallery="example-gallery">
                                        </div>
                                        <div class="carousel-item" data-slide-number="6">
                                            <img src="public/assets/img/bangpojan/8.jpg" class="d-block w-100" alt="..."
                                                data-remote="public/assets/img/bangpojan/8.jpg" data-type="image"
                                                data-toggle="lightbox" data-gallery="example-gallery">
                                        </div>
                                        <div class="carousel-item" data-slide-number="7">
                                            <img src="public/assets/img/bangpojan/9.jpg" class="d-block w-100" alt="..."
                                                data-remote="public/assets/img/bangpojan/9.jpg" data-type="image"
                                                data-toggle="lightbox" data-gallery="example-gallery">
                                        </div>
                                        <div class="carousel-item" data-slide-number="8">
                                            <img src="public/assets/img/bangpojan/10.jpeg" class="d-block w-100"
                                                alt="..." data-remote="public/assets/img/bangpojan/10.jpeg"
                                                data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
                                        </div>
                                        <div class="carousel-item" data-slide-number="9">
                                            <img src="public/assets/img/bangpojan/11.jpeg" class="d-block w-100"
                                                alt="..." data-remote="public/assets/img/bangpojan/11.jpeg"
                                                data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
                                        </div>
                                        <div class="carousel-item" data-slide-number="10">
                                            <img src="public/assets/img/bangpojan/16.jpeg" class="d-block w-100"
                                                alt="..." data-remote="public/assets/img/bangpojan/16.jpeg"
                                                data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
                                        </div>
                                        <div class="carousel-item" data-slide-number="11">
                                            <img src="public/assets/img/bangpojan/17.jpeg" class="d-block w-100"
                                                alt="..." data-remote="public/assets/img/bangpojan/17.jpeg"
                                                data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
                                        </div>
                                    </div>
                                </div>
                                <h1 style="display: none">cara bakar arang</h1>
                                <h1 style="display: none">charcoal briquettes</h1>
                                <h1 style="display: none">supplier arang</h1>
                                <h1 style="display: none">arang barbeque</h1>
                                <h1 style="display: none">coconut charcoal briquettes</h1>
                                <h1 style="display: none">coconut shell briquettes</h1>
                                <!-- Carousel Navigation -->
                                <div id="carousel-thumbs" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <div class="row mx-0">

                                                <div id="carousel-selector-1" class="thumb col-4 col-sm-2 px-1 py-2"
                                                    data-target="#myCarousel" data-slide-to="0">
                                                    <img src="public/assets/img/bangpojan/1.jpeg" class="img-fluid"
                                                        alt="...">
                                                </div>
                                                <div id="carousel-selector-2" class="thumb col-4 col-sm-2 px-1 py-2"
                                                    data-target="#myCarousel" data-slide-to="1">
                                                    <img src="public/assets/img/bangpojan/2.jpeg" class="img-fluid"
                                                        alt="...">
                                                </div>
                                                <div id="carousel-selector-3" class="thumb col-4 col-sm-2 px-1 py-2"
                                                    data-target="#myCarousel" data-slide-to="2">
                                                    <img src="public/assets/img/bangpojan/3.jpeg" class="img-fluid"
                                                        alt="...">
                                                </div>
                                                <div id="carousel-selector-4" class="thumb col-4 col-sm-2 px-1 py-2"
                                                    data-target="#myCarousel" data-slide-to="3">
                                                    <img src="public/assets/img/bangpojan/4.jpeg" class="img-fluid"
                                                        alt="...">
                                                </div>
                                                <div id="carousel-selector-5" class="thumb col-4 col-sm-2 px-1 py-2"
                                                    data-target="#myCarousel" data-slide-to="4">
                                                    <img src="public/assets/img/bangpojan/6.jpg" class="img-fluid"
                                                        alt="...">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="carousel-item">
                                            <div class="row mx-0">
                                                <div id="carousel-selector-6" class="thumb col-4 col-sm-2 px-1 py-2"
                                                    data-target="#myCarousel" data-slide-to="5">
                                                    <img src="public/assets/img/bangpojan/7.jpg" class="img-fluid"
                                                        alt="...">
                                                </div>
                                                <div id="carousel-selector-7" class="thumb col-4 col-sm-2 px-1 py-2"
                                                    data-target="#myCarousel" data-slide-to="6">
                                                    <img src="public/assets/img/bangpojan/8.jpg" class="img-fluid"
                                                        alt="...">
                                                </div>
                                                <div id="carousel-selector-8" class="thumb col-4 col-sm-2 px-1 py-2"
                                                    data-target="#myCarousel" data-slide-to="7">
                                                    <img src="public/assets/img/bangpojan/9.jpg" class="img-fluid"
                                                        alt="...">
                                                </div>
                                                <div id="carousel-selector-9" class="thumb col-4 col-sm-2 px-1 py-2"
                                                    data-target="#myCarousel" data-slide-to="8">
                                                    <img src="public/assets/img/bangpojan/10.jpeg" class="img-fluid"
                                                        alt="...">
                                                </div>
                                                <div id="carousel-selector-10" class="thumb col-4 col-sm-2 px-1 py-2"
                                                    data-target="#myCarousel" data-slide-to="9">
                                                    <img src="public/assets/img/bangpojan/11.jpeg" class="img-fluid"
                                                        alt="...">
                                                </div>
                                            </div>
                                        </div>
                                        <h1 style="display: none">cara bakar arang</h1>
                                        <h1 style="display: none">charcoal briquettes</h1>
                                        <h1 style="display: none">supplier arang</h1>
                                        <h1 style="display: none">arang barbeque</h1>
                                        <h1 style="display: none">coconut charcoal briquettes</h1>
                                        <h1 style="display: none">coconut shell briquettes</h1>
                                        <div class="carousel-item">
                                            <div class="row mx-0">
                                                <div id="carousel-selector-11" class="thumb col-4 col-sm-2 px-1 py-2"
                                                    data-target="#myCarousel" data-slide-to="10">
                                                    <img src="public/assets/img/bangpojan/16.jpeg" class="img-fluid"
                                                        alt="...">
                                                </div>
                                                <div id="carousel-selector-12" class="thumb col-4 col-sm-2 px-1 py-2"
                                                    data-target="#myCarousel" data-slide-to="11">
                                                    <img src="public/assets/img/bangpojan/17.jpeg" class="img-fluid"
                                                        alt="...">
                                                </div>
                                                <div class="col-2 px-1 py-2"></div>
                                                <div class="col-2 px-1 py-2"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carousel-thumbs" role="button"
                                        data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carousel-thumbs" role="button"
                                        data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <h1 style="display: none">cara bakar arang</h1>
                                <h1 style="display: none">charcoal briquettes</h1>
                                <h1 style="display: none">supplier arang</h1>
                                <h1 style="display: none">arang barbeque</h1>
                                <h1 style="display: none">coconut charcoal briquettes</h1>
                                <h1 style="display: none">coconut shell briquettes</h1>

                            </div> <!-- /row -->
                        </div> <!-- /container -->
                        <!-- <img style="background-image: linear-gradient(
0deg
, #000000 0%, #242323 50%, #000000 100%);
    border-radius: 20px;
    border: solid gold;" src="public/assets/img/4.png" alt=""> -->
                    </div><br>
                    <div class="mantep">
                        <a style="font-size:22px;" class="d-flex justify-content-center" href="">H.M.
                            <?= lang('Global.fauzan'); ?>, Lc MA.</a>
                    </div>


                </div>
                <?php if (WEB_LANG == 'id') { ?>

                <?php  } elseif (WEB_LANG == 'en') { ?>

                <?php  } else { ?>

                <?php } ?>
                <h1 style="display: none">cara bakar arang</h1>
                <h1 style="display: none">charcoal briquettes</h1>
                <h1 style="display: none">supplier arang</h1>
                <h1 style="display: none">arang barbeque</h1>
                <h1 style="display: none">coconut charcoal briquettes</h1>
                <h1 style="display: none">coconut shell briquettes</h1>
                <div class="col-lg-5 box">
                    <?php if (WEB_LANG == 'id') { ?>
                    <p class="description text-justify" style="font-weight: bold; font-size:20px; color:gold;">
                        <?= lang('Global.tkdd'); ?></p>
                    <p class="description text-justify" style="font-size:18px;"><?= lang('Global.tkd'); ?></p><br><br>
                    <?php  } elseif (WEB_LANG == 'en') { ?>
                    <p class="description text-justify" style="font-weight: bold; font-size:20px; color:gold;">
                        <?= lang('Global.tkdd'); ?></p>
                    <p class="description text-justify" style="font-size:18px;"><?= lang('Global.tkd'); ?></p><br><br>
                    <?php  } else { ?>
                    <p class="description text-right" style="font-weight: bold; font-size:20px; color:gold;">
                        <?= lang('Global.tkdd'); ?></p>
                    <p class="description text-right" style="font-size:18px;"><?= lang('Global.tkd'); ?></p><br><br>
                    <?php } ?>

                    <div class="row">
                        <div class="col col-md-6">
                            <img style="background-image: linear-gradient(0deg, #000000 0%, #242323 50%, #000000 100%);border-radius: 20px; border: solid gold; width:100%;"
                                src="public/assets/img/bangpojan/12.png" alt="">
                            <br>
                            <p class="text-center" style="font-size:16px;"><?= lang('Global.sandi'); ?></p>
                        </div>
                        <div class="col col-md-6">
                            <img style="background-image: linear-gradient(0deg, #000000 0%, #242323 50%, #000000 100%);border-radius: 20px; border: solid gold; width:100%;"
                                src="public/assets/img/bangpojan/13.png" alt="">
                            <br>
                            <p class="text-center" style="font-size:16px;"><?= lang('Global.bkpm'); ?></p>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col col-md-6">
                            <img style="background-image: linear-gradient(0deg, #000000 0%, #242323 50%, #000000 100%);border-radius: 20px; border: solid gold; width:100%;"
                                src="public/assets/img/bangpojan/14.png" alt="">
                            <br>
                            <p class="text-center" style="font-size:16px;"><?= lang('Global.dubesarab1'); ?></p>
                        </div>

                        <div class="col col-md-6">
                            <img style="background-image: linear-gradient(0deg, #000000 0%, #242323 50%, #000000 100%);border-radius: 20px; border: solid gold; width:100%;"
                                src="public/assets/img/bangpojan/15.png" alt="">
                            <br>
                            <p class="text-center" style="font-size:16px;"><?= lang('Global.dubesarab2'); ?></p>
                        </div>
                    </div>
                </div>
                <h1 style="display: none">cara bakar arang</h1>
                <h1 style="display: none">charcoal briquettes</h1>
                <h1 style="display: none">supplier arang</h1>
                <h1 style="display: none">arang barbeque</h1>
                <h1 style="display: none">coconut charcoal briquettes</h1>
                <h1 style="display: none">coconut shell briquettes</h1>

                <div class="col-lg-2 box text-center">
                </div>

            </div>
        </div>
    </section><!-- #featured-services -->

    <section id="featured-services">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-5 box text-center">



                </div>

                <div class="col-lg-5 box text-center">

                </div>

                <div class="col-lg-2 box text-center">
                </div>

            </div>
        </div>
    </section><!-- #featured-services -->

    <!--==========================
      About Us Section
    ============================-->
    <section id="about">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-5 box text-center">
                    <?php if (WEB_LANG == 'id') { ?>
                    <h4 class="title text-left"><a href="" style="color: gold;"><?= lang('Global.lbc1'); ?></a></h4>
                    <?php  } elseif (WEB_LANG == 'en') { ?>
                    <h4 class="title text-left"><a href="" style="color: gold;"><?= lang('Global.lbc1'); ?></a></h4>
                    <?php  } else { ?>
                    <h4 class="title text-right"><a href="" style="color: gold;"><?= lang('Global.lbc1'); ?></a></h4>
                    <?php } ?>


                    <hr class='dotted' />

                </div>

                <div class="col-lg-5 box text-center">
                    <p class="description text-justify text-white">
                        <?= lang('Global.lbc2'); ?>
                    </p>

                    <p class="description text-justify text-white">
                        <?= lang('Global.lbc3'); ?>
                    </p>
                </div>

                <div class="col-lg-2 box text-center">
                </div>

            </div>
        </div>
    </section><!-- #about -->

    <!--==========================
      Services Section
    ============================-->
    <!-- <section id="services">
        <div class="container">

            <header class="section-header wow fadeInUp">
                <h3>Services</h3>
                <p>Laudem latine persequeris id sed, ex fabulas delectus quo. No vel partiendo abhorreant vituperatoribus, ad pro quaestio laboramus. Ei ubique vivendum pro. At ius nisl accusam lorenta zanos paradigno tridexa panatarel.</p>
            </header>

            <div class="row">

                <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
                    <div class="icon"><i class="ion-ios-analytics-outline"></i></div>
                    <h4 class="title"><a href="">Lorem Ipsum</a></h4>
                    <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
                </div>
                <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
                    <div class="icon"><i class="ion-ios-bookmarks-outline"></i></div>
                    <h4 class="title"><a href="">Dolor Sitema</a></h4>
                    <p class="description">Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata</p>
                </div>
                <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
                    <div class="icon"><i class="ion-ios-paper-outline"></i></div>
                    <h4 class="title"><a href="">Sed ut perspiciatis</a></h4>
                    <p class="description">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</p>
                </div>
                <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                    <div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
                    <h4 class="title"><a href="">Magni Dolores</a></h4>
                    <p class="description">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
                </div>
                <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                    <div class="icon"><i class="ion-ios-barcode-outline"></i></div>
                    <h4 class="title"><a href="">Nemo Enim</a></h4>
                    <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
                </div>
                <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                    <div class="icon"><i class="ion-ios-people-outline"></i></div>
                    <h4 class="title"><a href="">Eiusmod Tempor</a></h4>
                    <p class="description">Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi</p>
                </div>

            </div>

        </div>
    </section>#services -->

    <section id="featured-services">
        <div class="container">
            <div class="row">



                <div class="col-lg-4 box text-center">

                    <img style="background-image: linear-gradient(0deg, #000000 0%, #242323 50%, #000000 100%);border-radius: 20px; border: solid gold; width:100%;"
                        src="public/assets/img/bangpojan/21.jpeg" alt="" style="width: 100%;">

                </div>

                <div class="col-lg-4 box text-center">

                    <img style="background-image: linear-gradient(0deg, #000000 0%, #242323 50%, #000000 100%);border-radius: 20px; border: solid gold; width:100%;"
                        src="public/assets/img/bangpojan/20.jpeg" alt="" style="width: 100%;">

                </div>
                <div class="col-lg-4 box text-center">

                    <img style="background-image: linear-gradient(0deg, #000000 0%, #242323 50%, #000000 100%);border-radius: 20px; border: solid gold; width:100%;"
                        src="public/assets/img/bangpojan/23.jpeg" alt="" style="width: 100%;">

                </div>




            </div>
        </div>
    </section><!-- #featured-services -->

    <!--==========================
      Call To Action Section
    ============================-->
    <!-- <section id="call-to-action" class="wow fadeIn">
        <div class="container text-center">
            <h3>Call To Action</h3>
            <p> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            <a class="cta-btn" href="#">Call To Action</a>
        </div>
    </section>#call-to-action -->

    <!--==========================
      Skills Section
    ============================-->
    <!-- <section id="skills">
        <div class="container">

            <header class="section-header">
                <h3>Our Skills</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
            </header>

            <div class="skills-content">

                <div class="progress">
                    <div class="progress-bar bg-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                        <span class="skill">HTML <i class="val">100%</i></span>
                    </div>
                </div>

                <div class="progress">
                    <div class="progress-bar bg-info" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
                        <span class="skill">CSS <i class="val">90%</i></span>
                    </div>
                </div>

                <div class="progress">
                    <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">
                        <span class="skill">JavaScript <i class="val">75%</i></span>
                    </div>
                </div>

                <div class="progress">
                    <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100">
                        <span class="skill">Photoshop <i class="val">55%</i></span>
                    </div>
                </div>

            </div>

        </div>
    </section> -->

    <!--==========================
      Facts Section
    ============================-->
    <!-- <section id="facts" class="wow fadeIn">
        <div class="container">

            <header class="section-header">
                <h3>Facts</h3>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p>
            </header>

            <div class="row counters">

                <div class="col-lg-3 col-6 text-center">
                    <span data-toggle="counter-up">274</span>
                    <p>Clients</p>
                </div>

                <div class="col-lg-3 col-6 text-center">
                    <span data-toggle="counter-up">421</span>
                    <p>Projects</p>
                </div>

                <div class="col-lg-3 col-6 text-center">
                    <span data-toggle="counter-up">1,364</span>
                    <p>Hours Of Support</p>
                </div>

                <div class="col-lg-3 col-6 text-center">
                    <span data-toggle="counter-up">18</span>
                    <p>Hard Workers</p>
                </div>

            </div>

            <div class="facts-img">
                <img src="img/facts-img.png" alt="" class="img-fluid">
            </div>

        </div>
    </section>#facts -->

    <!--==========================
      Portfolio Section
    ============================-->
    <!-- <section id="portfolio" class="section-bg">
        <div class="container">

            <header class="section-header">
                <h3 class="section-title">Our Portfolio</h3>
            </header>

            <div class="row">
                <div class="col-lg-12">
                    <ul id="portfolio-flters">
                        <li data-filter="*" class="filter-active">All</li>
                        <li data-filter=".filter-app">App</li>
                        <li data-filter=".filter-card">Card</li>
                        <li data-filter=".filter-web">Web</li>
                    </ul>
                </div>
            </div>

            <div class="row portfolio-container">

                <div class="col-lg-4 col-md-6 portfolio-item filter-app wow fadeInUp">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="img/portfolio/app1.jpg" class="img-fluid" alt="">
                            <a href="img/portfolio/app1.jpg" data-lightbox="portfolio" data-title="App 1" class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">App 1</a></h4>
                            <p>App</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.1s">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="img/portfolio/web3.jpg" class="img-fluid" alt="">
                            <a href="img/portfolio/web3.jpg" class="link-preview" data-lightbox="portfolio" data-title="Web 3" title="Preview"><i class="ion ion-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">Web 3</a></h4>
                            <p>Web</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-app wow fadeInUp" data-wow-delay="0.2s">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="img/portfolio/app2.jpg" class="img-fluid" alt="">
                            <a href="img/portfolio/app2.jpg" class="link-preview" data-lightbox="portfolio" data-title="App 2" title="Preview"><i class="ion ion-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">App 2</a></h4>
                            <p>App</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-card wow fadeInUp">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="img/portfolio/card2.jpg" class="img-fluid" alt="">
                            <a href="img/portfolio/card2.jpg" class="link-preview" data-lightbox="portfolio" data-title="Card 2" title="Preview"><i class="ion ion-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">Card 2</a></h4>
                            <p>Card</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.1s">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="img/portfolio/web2.jpg" class="img-fluid" alt="">
                            <a href="img/portfolio/web2.jpg" class="link-preview" data-lightbox="portfolio" data-title="Web 2" title="Preview"><i class="ion ion-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">Web 2</a></h4>
                            <p>Web</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-app wow fadeInUp" data-wow-delay="0.2s">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="img/portfolio/app3.jpg" class="img-fluid" alt="">
                            <a href="img/portfolio/app3.jpg" class="link-preview" data-lightbox="portfolio" data-title="App 3" title="Preview"><i class="ion ion-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">App 3</a></h4>
                            <p>App</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-card wow fadeInUp">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="img/portfolio/card1.jpg" class="img-fluid" alt="">
                            <a href="img/portfolio/card1.jpg" class="link-preview" data-lightbox="portfolio" data-title="Card 1" title="Preview"><i class="ion ion-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">Card 1</a></h4>
                            <p>Card</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-card wow fadeInUp" data-wow-delay="0.1s">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="img/portfolio/card3.jpg" class="img-fluid" alt="">
                            <a href="img/portfolio/card3.jpg" class="link-preview" data-lightbox="portfolio" data-title="Card 3" title="Preview"><i class="ion ion-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">Card 3</a></h4>
                            <p>Card</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.2s">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="img/portfolio/web1.jpg" class="img-fluid" alt="">
                            <a href="img/portfolio/web1.jpg" class="link-preview" data-lightbox="portfolio" data-title="Web 1" title="Preview"><i class="ion ion-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">Web 1</a></h4>
                            <p>Web</p>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>#portfolio -->

    <!--==========================
      Clients Section
    ============================-->
    <!-- <section id="clients" class="wow fadeInUp">
        <div class="container">

            <header class="section-header">
                <h3>Our Clients</h3>
            </header>

            <div class="owl-carousel clients-carousel">
                <img src="img/clients/client-1.png" alt="">
                <img src="img/clients/client-2.png" alt="">
                <img src="img/clients/client-3.png" alt="">
                <img src="img/clients/client-4.png" alt="">
                <img src="img/clients/client-5.png" alt="">
                <img src="img/clients/client-6.png" alt="">
                <img src="img/clients/client-7.png" alt="">
                <img src="img/clients/client-8.png" alt="">
            </div>

        </div>
    </section>#clients -->

    <!--==========================
      Clients Section
    ============================-->
    <!-- <section id="testimonials" class="section-bg wow fadeInUp">
        <div class="container">

            <header class="section-header">
                <h3>Testimonials</h3>
            </header>

            <div class="owl-carousel testimonials-carousel">

                <div class="testimonial-item">
                    <img src="img/testimonial-1.jpg" class="testimonial-img" alt="">
                    <h3>Saul Goodman</h3>
                    <h4>Ceo &amp; Founder</h4>
                    <p>
                        <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
                        Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
                        <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
                    </p>
                </div>

                <div class="testimonial-item">
                    <img src="img/testimonial-2.jpg" class="testimonial-img" alt="">
                    <h3>Sara Wilsson</h3>
                    <h4>Designer</h4>
                    <p>
                        <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
                        Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim culpa.
                        <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
                    </p>
                </div>

                <div class="testimonial-item">
                    <img src="img/testimonial-3.jpg" class="testimonial-img" alt="">
                    <h3>Jena Karlis</h3>
                    <h4>Store Owner</h4>
                    <p>
                        <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
                        Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem veniam duis minim tempor labore quem eram duis noster aute amet eram fore quis sint minim.
                        <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
                    </p>
                </div>

                <div class="testimonial-item">
                    <img src="img/testimonial-4.jpg" class="testimonial-img" alt="">
                    <h3>Matt Brandon</h3>
                    <h4>Freelancer</h4>
                    <p>
                        <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
                        Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim fugiat minim velit minim dolor enim duis veniam ipsum anim magna sunt elit fore quem dolore labore illum veniam.
                        <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
                    </p>
                </div>

                <div class="testimonial-item">
                    <img src="img/testimonial-5.jpg" class="testimonial-img" alt="">
                    <h3>John Larson</h3>
                    <h4>Entrepreneur</h4>
                    <p>
                        <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
                        Quis quorum aliqua sint quem legam fore sunt eram irure aliqua veniam tempor noster veniam enim culpa labore duis sunt culpa nulla illum cillum fugiat legam esse veniam culpa fore nisi cillum quid.
                        <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
                    </p>
                </div>

            </div>

        </div>
    </section>#testimonials -->

    <!--==========================
      Team Section
    ============================-->
    <!-- <section id="team">
        <div class="container">
            <div class="section-header wow fadeInUp">
                <h3>Team</h3>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p>
            </div>

            <div class="row">

                <div class="col-lg-3 col-md-6 wow fadeInUp">
                    <div class="member">
                        <img src="img/team-1.jpg" class="img-fluid" alt="">
                        <div class="member-info">
                            <div class="member-info-content">
                                <h4>Walter White</h4>
                                <span>Chief Executive Officer</span>
                                <div class="social">
                                    <a href=""><i class="fa fa-twitter"></i></a>
                                    <a href=""><i class="fa fa-facebook"></i></a>
                                    <a href=""><i class="fa fa-google-plus"></i></a>
                                    <a href=""><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="member">
                        <img src="img/team-2.jpg" class="img-fluid" alt="">
                        <div class="member-info">
                            <div class="member-info-content">
                                <h4>Sarah Jhonson</h4>
                                <span>Product Manager</span>
                                <div class="social">
                                    <a href=""><i class="fa fa-twitter"></i></a>
                                    <a href=""><i class="fa fa-facebook"></i></a>
                                    <a href=""><i class="fa fa-google-plus"></i></a>
                                    <a href=""><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                    <div class="member">
                        <img src="img/team-3.jpg" class="img-fluid" alt="">
                        <div class="member-info">
                            <div class="member-info-content">
                                <h4>William Anderson</h4>
                                <span>CTO</span>
                                <div class="social">
                                    <a href=""><i class="fa fa-twitter"></i></a>
                                    <a href=""><i class="fa fa-facebook"></i></a>
                                    <a href=""><i class="fa fa-google-plus"></i></a>
                                    <a href=""><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                    <div class="member">
                        <img src="img/team-4.jpg" class="img-fluid" alt="">
                        <div class="member-info">
                            <div class="member-info-content">
                                <h4>Amanda Jepson</h4>
                                <span>Accountant</span>
                                <div class="social">
                                    <a href=""><i class="fa fa-twitter"></i></a>
                                    <a href=""><i class="fa fa-facebook"></i></a>
                                    <a href=""><i class="fa fa-google-plus"></i></a>
                                    <a href=""><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>#team -->

    <!--==========================
      Contact Section
    ============================-->
    <!-- <section id="contact" class="section-bg wow fadeInUp">
        <div class="container">

            <div class="section-header">
                <h3>Contact Us</h3>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p>
            </div>

            <div class="row contact-info">

                <div class="col-md-4">
                    <div class="contact-address">
                        <i class="ion-ios-location-outline"></i>
                        <h3>Address</h3>
                        <address>A108 Adam Street, NY 535022, USA</address>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="contact-phone">
                        <i class="ion-ios-telephone-outline"></i>
                        <h3>Phone Number</h3>
                        <p><a href="tel:+155895548855">+1 5589 55488 55</a></p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="contact-email">
                        <i class="ion-ios-email-outline"></i>
                        <h3>Email</h3>
                        <p><a href="mailto:info@example.com">info@example.com</a></p>
                    </div>
                </div>

            </div>

            <div class="form">
                <div id="sendmessage">Your message has been sent. Thank you!</div>
                <div id="errormessage"></div>
                <form action="" method="post" role="form" class="contactForm">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                            <div class="validation"></div>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                            <div class="validation"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                        <div class="validation"></div>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                        <div class="validation"></div>
                    </div>
                    <div class="text-center"><button type="submit">Send Message</button></div>
                </form>
            </div>

        </div>
    </section>#contact -->
    <script>
    $('#myCarousel').carousel({
        interval: false
    });
    $('#carousel-thumbs').carousel({
        interval: false
    });

    // handles the carousel thumbnails
    // https://stackoverflow.com/questions/25752187/bootstrap-carousel-with-thumbnails-multiple-carousel
    $('[id^=carousel-selector-]').click(function() {
        var id_selector = $(this).attr('id');
        var id = parseInt(id_selector.substr(id_selector.lastIndexOf('-') + 1));
        $('#myCarousel').carousel(id);
    });
    // Only display 3 items in nav on mobile.
    if ($(window).width() < 575) {
        $('#carousel-thumbs .row div:nth-child(4)').each(function() {
            var rowBoundary = $(this);
            $('<div class="row mx-0">').insertAfter(rowBoundary.parent()).append(rowBoundary.nextAll()
                .addBack());
        });
        $('#carousel-thumbs .carousel-item .row:nth-child(even)').each(function() {
            var boundary = $(this);
            $('<div class="carousel-item">').insertAfter(boundary.parent()).append(boundary.nextAll()
                .addBack());
        });
    }
    // Hide slide arrows if too few items.
    if ($('#carousel-thumbs .carousel-item').length < 2) {
        $('#carousel-thumbs [class^=carousel-control-]').remove();
        $('.machine-carousel-container #carousel-thumbs').css('padding', '0 5px');
    }
    // when the carousel slides, auto update
    $('#myCarousel').on('slide.bs.carousel', function(e) {
        var id = parseInt($(e.relatedTarget).attr('data-slide-number'));
        $('[id^=carousel-selector-]').removeClass('selected');
        $('[id=carousel-selector-' + id + ']').addClass('selected');
    });
    // when user swipes, go next or previous
    $('#myCarousel').swipe({
        fallbackToMouseEvents: true,
        swipeLeft: function(e) {
            $('#myCarousel').carousel('next');
        },
        swipeRight: function(e) {
            $('#myCarousel').carousel('prev');
        },
        allowPageScroll: 'vertical',
        preventDefaultEvents: false,
        threshold: 75
    });
    /*
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox();
    });
    */

    $('#myCarousel .carousel-item img').on('click', function(e) {
        var src = $(e.target).attr('data-remote');
        if (src) $(this).ekkoLightbox();
    });
    </script>
</main>

<?= $this->endSection(); ?>
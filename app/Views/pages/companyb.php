<?= $this->extend('template/layout'); ?>
<?= $this->section('content'); ?>
<style>
.dotted {
    border: 6px dotted #ffffff;
    border-style: none none dotted;
    color: #fff;
}

#featured-services .box-bg1 {
    background-image: linear-gradient(0deg, #000000 0%, #242323 50%, #000000 100%);
    border-radius: 20px;
    border: solid gold;
    padding: 10px 10px 10px 10px;

}

.mantep>a {
    color: white;
    padding-left: 200px;

}

.carousel {
    position: relative;
}

.carousel-item img {
    object-fit: cover;
}

#carousel-thumbs {
    background: rgba(255, 255, 255, .3);
    bottom: 0;
    left: 0;
    padding: 0 50px;
    right: 0;
}

#carousel-thumbs img {
    border: 5px solid transparent;
    cursor: pointer;
}

#carousel-thumbs img:hover {
    border-color: rgba(255, 255, 255, .3);
}

#carousel-thumbs .selected img {
    border-color: #fff;
}

.carousel-control-prev,
.carousel-control-next {
    width: 50px;
}

@media all and (max-width: 767px) {
    .carousel-container #carousel-thumbs img {
        border-width: 3px;
    }
}

@media all and (min-width: 576px) {
    .carousel-container #carousel-thumbs {
        position: absolute;
    }
}

@media all and (max-width: 576px) {
    .carousel-container #carousel-thumbs {
        background: #ccccce;
    }
}

@media (max-width: 768px) {
    .mantep>a {
        color: white;
        padding-left: 0px;

    }

    #intro .carousel-item {
        width: 100%;
        height: 30vh;
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
    }
}
</style>
<section id="intro" style="height: auto;">
    <h1 style="display: none">cara bakar arang</h1>
    <h1 style="display: none">charcoal briquettes</h1>
    <h1 style="display: none">supplier arang</h1>
    <h1 style="display: none">arang barbeque</h1>
    <h1 style="display: none">coconut charcoal briquettes</h1>
    <h1 style="display: none">coconut shell briquettes</h1>
    <img src="public/assets/img/companybgg.png" alt="" style="width: 100%;">
    <!-- <h1 style="font-weight: bolder;">COMPANY</h1> -->

</section><!-- #intro -->

<main id="main">


    <!--==========================
      Featured Services Section
    ============================-->
    <!-- <section id="featured-services">
        <div class="container">
            <div class="row">

                <div class="col-lg-12 text-center">

                    <img src="public/assets/img/box5.png" alt="" style="width: 100%;">

                </div>


            </div>
        </div>
    </section>#featured-services -->

    <!--==========================
      Featured Services Section
    ============================-->
    <style>
    #featured-services p {
        font-size: 16px;
        color: #fff;
    }

    #about p {
        font-size: 16px;
        color: #fff;
    }
    </style>
    <?php if (WEB_LANG == 'ar') {
        echo '  <style>#featured-services p {font-size: 22px;

            color: #fff;
            }

            #about p {font-size: 22px;

                color: #fff;
                }</style>';
    } ?>

    <!-- <section id="featured-services">
        <div class="container">
            <div class="row">

                <div class="col-lg-4 box text-center">
                    <div class="">
                        <img style="background-image: linear-gradient(
0deg
, #000000 0%, #242323 50%, #000000 100%);
    border-radius: 20px;
    border: solid gold; width:100%;" src="public/assets/img/1.png" alt="">
                    </div>
                </div>

                <div class="col-lg-4 box text-center">
                    <div class="">
                        <img style="background-image: linear-gradient(
0deg
, #000000 0%, #242323 50%, #000000 100%);
    border-radius: 20px;
    border: solid gold; width:100%;" src="public/assets/img/2.png" alt="">
                    </div>
                </div>

                <div class="col-lg-4 box text-center">
                    <div class="">
                        <img style="background-image: linear-gradient(
0deg
, #000000 0%, #242323 50%, #000000 100%);
    border-radius: 20px;
    border: solid gold; width:100%;" src="public/assets/img/3.png" alt="">
                    </div>
                </div>


            </div>
        </div>
    </section> -->
    <section id="featured-services">
        <div class="container-fluid">
            <div class="row d-flex justify-content-center mb-5">
                <div class="col col-md-2">
                    <img style="width:200px;" src="public/assets/img/testt.png" alt="">
                </div>
            </div>
            <div class="row">
                <div class="col col-lg-5 col-sm-6">
                    <div class="row">
                        <div class="col col-md-6">
                            <img style="background-image: linear-gradient(0deg, #000000 0%, #242323 50%, #000000 100%);border-radius: 20px; border: solid gold; width:100%;"
                                src="public/assets/img/pabrik1.jpeg" alt="" style="width: 100%;">
                            <br><br>
                            <img style="background-image: linear-gradient(0deg, #000000 0%, #242323 50%, #000000 100%);border-radius: 20px; border: solid gold; width:100%;"
                                src="public/assets/img/pabrik2.jpeg" alt="" style="width: 100%;">
                            <br><br>
                            <img style="background-image: linear-gradient(0deg, #000000 0%, #242323 50%, #000000 100%);border-radius: 20px; border: solid gold; width:100%;"
                                src="public/assets/img/pabrik5.jpeg" alt="" style="width: 100%;">

                        </div>
                        <div class="col col-md-6">
                            <img style="background-image: linear-gradient(0deg, #000000 0%, #242323 50%, #000000 100%);border-radius: 20px; border: solid gold; width:100%;"
                                src="public/assets/img/pabrik3.jpeg" alt="" style="width: 100%;">
                            <br><br>
                            <img style="background-image: linear-gradient(0deg, #000000 0%, #242323 50%, #000000 100%);border-radius: 20px; border: solid gold; width:100%;"
                                src="public/assets/img/pabrik4.jpeg" alt="" style="width: 100%;">

                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col">
                            <div class="text-right">

                                <?php if (WEB_LANG == 'id') {
                                    echo '<img src="public/assets/img/tulisan2.png" alt="">';
                                } elseif (WEB_LANG == 'en') {
                                    echo '<img src="public/assets/img/tulisan2inggris.png" alt="">';
                                } else { ?>

                                <style>
                                .arabs>p {
                                    width: 279px;
                                    margin-right: 0px;
                                    margin-left: 260px;
                                }

                                @media (max-width: 768px) {
                                    .arabs>p {
                                        margin-right: 0px;
                                        margin-left: 50px;
                                        padding-bottom: 40px;
                                        width: 270px;
                                    }
                                }
                                </style>
                                <div class="arabs">
                                    <p style="font-size: 24px;font-weight: bold;color:gold;">شركة طيبة للفحم
                                        موجودة لتلبية احتياجات
                                        واستهلاك فحم قشرة جوز الهند
                                        داخل إندونيسيا وخارجها
                                    </p>
                                </div>
                                <?php } ?>


                            </div>
                        </div>
                        <h1 style="display: none">cara bakar arang</h1>
                        <h1 style="display: none">charcoal briquettes</h1>
                        <h1 style="display: none">supplier arang</h1>
                        <h1 style="display: none">arang barbeque</h1>
                        <h1 style="display: none">coconut charcoal briquettes</h1>
                        <h1 style="display: none">coconut shell briquettes</h1>
                    </div>
                </div>
                <div class="col col-lg-5 col-sm-6">
                    <?php if (WEB_LANG == 'id') { ?>
                    <p style="font-weight: bold; font-size:20px; color:gold; text-align:justify;">
                        <?= lang('Global.lbcomj'); ?></p>
                    <p class="description text-justify"><?= lang('Global.lbcom'); ?></p><br>
                    <p class="description text-justify"><?= lang('Global.lbcom2'); ?></p><br>
                    <p class="description text-justify"><?= lang('Global.lbcom3'); ?></p><br>
                    <?php  } elseif (WEB_LANG == 'en') { ?>
                    <p style="font-weight: bold; font-size:20px; color:gold; text-align:justify;">
                        <?= lang('Global.lbcomj'); ?></p>
                    <p class="description text-justify"><?= lang('Global.lbcom'); ?></p><br>
                    <p class="description text-justify"><?= lang('Global.lbcom2'); ?></p><br>
                    <p class="description text-justify"><?= lang('Global.lbcom3'); ?></p><br>
                    <?php  } else { ?>
                    <p style="font-weight: bold; font-size:20px; color:gold; text-align:right;">
                        <?= lang('Global.lbcomj'); ?></p>
                    <p class="description text-right"><?= lang('Global.lbcom'); ?></p><br>
                    <p class="description text-right"><?= lang('Global.lbcom2'); ?></p><br>
                    <p class="description text-right"><?= lang('Global.lbcom3'); ?></p><br>
                    <?php } ?>

                </div>

                <div class="col-lg-2 box text-center">
                </div>

            </div>
        </div>
    </section><!-- #featured-services -->

    <section id="featured-services">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-5 box text-center">
                    <?php if (WEB_LANG == 'id') { ?>
                    <h4 class="title text-left"><a href=""><?= lang('Global.vm'); ?></a></h4>
                    <?php  } elseif (WEB_LANG == 'en') { ?>
                    <h4 class="title text-left"><a href=""><?= lang('Global.vm'); ?></a></h4>
                    <?php  } else { ?>
                    <h4 class="title text-right"><a href=""><?= lang('Global.vm'); ?></a></h4>
                    <?php } ?>


                    <h1 style="display: none">cara bakar arang</h1>
                    <h1 style="display: none">charcoal briquettes</h1>
                    <h1 style="display: none">supplier arang</h1>
                    <h1 style="display: none">arang barbeque</h1>
                    <h1 style="display: none">coconut charcoal briquettes</h1>
                    <h1 style="display: none">coconut shell briquettes</h1>
                    <style>
                    .dotted {
                        border: 6px dotted #ffffff;
                        border-style: none none dotted;
                        color: #fff;
                    }
                    </style>
                    <hr class='dotted' />
                    <br><br>
                    <div class="row">
                        <div class="col col-md-12">
                            <img style="background-image: linear-gradient(0deg, #000000 0%, #242323 50%, #000000 100%);border-radius: 20px; border: solid gold; width:82%;"
                                src="public/assets/img/bangpojan/24.jpeg" alt="">
                        </div>
                        <!-- <div class="col col-md-6">
                            <img style="background-image: linear-gradient(0deg, #000000 0%, #242323 50%, #000000 100%);border-radius: 20px; border: solid gold; width:100%;" src="public/assets/img/bangpojan/26.jpeg" alt="">
                        </div> -->
                    </div>
                </div>
                <h1 style="display: none">cara bakar arang</h1>
                <h1 style="display: none">charcoal briquettes</h1>
                <h1 style="display: none">supplier arang</h1>
                <h1 style="display: none">arang barbeque</h1>
                <h1 style="display: none">coconut charcoal briquettes</h1>
                <h1 style="display: none">coconut shell briquettes</h1>

                <div class="col-lg-5 box text-center">
                    <?php if (WEB_LANG == 'id') { ?>
                    <p class="description text-justify" style="font-size:26px; color:gold;">
                        <?= lang('Global.vm1'); ?>
                    </p>
                    <br>
                    <p class="description text-justify">
                        <?= lang('Global.vm2'); ?>

                    </p>
                    <br>
                    <p class="description text-justify" style="font-size:26px; color:gold;">
                        <?= lang('Global.vm3'); ?>

                    </p>
                    <br>
                    <p class="description text-justify">
                        <?= lang('Global.vm4'); ?>

                    </p>
                    <p class="description text-justify">
                        <?= lang('Global.vm5'); ?>

                    </p>
                    <p class="description text-justify">
                        <?= lang('Global.vm6'); ?>
                    </p>
                    <?php  } elseif (WEB_LANG == 'en') { ?>
                    <p class="description text-justify" style="font-size:26px; color:gold;">
                        <?= lang('Global.vm1'); ?>
                    </p>
                    <br>
                    <p class="description text-justify">
                        <?= lang('Global.vm2'); ?>

                    </p>
                    <br>
                    <p class="description text-justify" style="font-size:26px; color:gold;">
                        <?= lang('Global.vm3'); ?>

                    </p>
                    <br>
                    <p class="description text-justify">
                        <?= lang('Global.vm4'); ?>

                    </p>
                    <p class="description text-justify">
                        <?= lang('Global.vm5'); ?>

                    </p>
                    <p class="description text-justify">
                        <?= lang('Global.vm6'); ?>
                    </p>
                    <?php  } else { ?>
                    <p class="description text-right" style="font-size:26px; color:gold;">
                        <?= lang('Global.vm1'); ?>
                    </p>
                    <br>
                    <p class="description text-right">
                        <?= lang('Global.vm2'); ?>

                    </p>
                    <br>
                    <p class="description text-right" style="font-size:26px; color:gold;">
                        <?= lang('Global.vm3'); ?>

                    </p>
                    <br>
                    <p class="description text-right">
                        <?= lang('Global.vm4'); ?>

                    </p>
                    <p class="description text-right">
                        <?= lang('Global.vm5'); ?>

                    </p>
                    <p class="description text-right">
                        <?= lang('Global.vm6'); ?>
                    </p>
                    <?php } ?>

                </div>

                <div class="col-lg-2 box text-center">
                </div>

            </div>
        </div>
    </section><!-- #featured-services -->
    <h1 style="display: none">cara bakar arang</h1>
    <h1 style="display: none">charcoal briquettes</h1>
    <h1 style="display: none">supplier arang</h1>
    <h1 style="display: none">arang barbeque</h1>
    <h1 style="display: none">coconut charcoal briquettes</h1>
    <h1 style="display: none">coconut shell briquettes</h1>
    <!--==========================
      About Us Section
    ============================-->
    <section id="about">
        <div class="container-fluid">
            <div class="row">


                <div class="col-lg-5 box text-center">
                    <?php if (WEB_LANG == 'id') { ?>
                    <h4 class="title text-left"><a href="" style="color: gold;"><?= lang('Global.lbc1'); ?></a></h4>
                    <?php  } elseif (WEB_LANG == 'en') { ?>
                    <h4 class="title text-left"><a href="" style="color: gold;"><?= lang('Global.lbc1'); ?></a></h4>
                    <?php  } else { ?>
                    <h4 class="title text-right"><a href="" style="color: gold;"><?= lang('Global.lbc1'); ?></a></h4>
                    <?php } ?>



                    <hr class='dotted' />

                </div>
                <?php if (WEB_LANG == 'id') { ?>

                <?php  } elseif (WEB_LANG == 'en') { ?>

                <?php  } else { ?>

                <?php } ?>

                <div class="col-lg-5 box text-right">
                    <?php if (WEB_LANG == 'id') { ?>
                    <p class="description text-justify text-white">
                        <?= lang('Global.lbc2'); ?>
                    </p>

                    <p class="description text-justify text-white">
                        <?= lang('Global.lbc3'); ?>
                    </p>
                    <?php  } elseif (WEB_LANG == 'en') { ?>
                    <p class="description text-justify text-white">
                        <?= lang('Global.lbc2'); ?>
                    </p>

                    <p class="description text-justify text-white">
                        <?= lang('Global.lbc3'); ?>
                    </p>
                    <?php  } else { ?>
                    <p class="description text-right text-white">
                        <?= lang('Global.lbc2'); ?>
                    </p>

                    <p class="description text-right text-white">
                        <?= lang('Global.lbc3'); ?>
                    </p>
                    <?php } ?>

                </div>

                <div class="col-lg-2 box text-center">
                </div>

            </div>
        </div>
    </section><!-- #about -->

    <!--==========================
      Services Section
    ============================-->
    <!-- <section id="services">
        <div class="container">

            <header class="section-header wow fadeInUp">
                <h3>Services</h3>
                <p>Laudem latine persequeris id sed, ex fabulas delectus quo. No vel partiendo abhorreant vituperatoribus, ad pro quaestio laboramus. Ei ubique vivendum pro. At ius nisl accusam lorenta zanos paradigno tridexa panatarel.</p>
            </header>

            <div class="row">

                <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
                    <div class="icon"><i class="ion-ios-analytics-outline"></i></div>
                    <h4 class="title"><a href="">Lorem Ipsum</a></h4>
                    <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
                </div>
                <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
                    <div class="icon"><i class="ion-ios-bookmarks-outline"></i></div>
                    <h4 class="title"><a href="">Dolor Sitema</a></h4>
                    <p class="description">Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata</p>
                </div>
                <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
                    <div class="icon"><i class="ion-ios-paper-outline"></i></div>
                    <h4 class="title"><a href="">Sed ut perspiciatis</a></h4>
                    <p class="description">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</p>
                </div>
                <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                    <div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
                    <h4 class="title"><a href="">Magni Dolores</a></h4>
                    <p class="description">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
                </div>
                <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                    <div class="icon"><i class="ion-ios-barcode-outline"></i></div>
                    <h4 class="title"><a href="">Nemo Enim</a></h4>
                    <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
                </div>
                <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
                    <div class="icon"><i class="ion-ios-people-outline"></i></div>
                    <h4 class="title"><a href="">Eiusmod Tempor</a></h4>
                    <p class="description">Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi</p>
                </div>

            </div>

        </div>
    </section>#services -->

    <section id="featured-services">
        <div class="container">
            <div class="row">

                <div class="col-lg-3 box text-center">
                    <img style="background-image: linear-gradient(0deg, #000000 0%, #242323 50%, #000000 100%);border-radius: 20px; border: solid gold; width:100%;"
                        src="public/assets/img/jokw1.jpeg" alt="">


                </div>

                <div class="col-lg-3 box text-center">

                    <img style="background-image: linear-gradient(0deg, #000000 0%, #242323 50%, #000000 100%);border-radius: 20px; border: solid gold; width:100%;"
                        src="public/assets/img/pabrik9.jpeg" alt="">

                </div>

                <div class="col-lg-3 box text-center">

                    <img style="background-image: linear-gradient(0deg, #000000 0%, #242323 50%, #000000 100%);border-radius: 20px; border: solid gold; width:100%;"
                        src="public/assets/img/pabrik6.jpeg" alt="">


                </div>

                <div class="col-lg-3 box text-center">

                    <img style="background-image: linear-gradient(0deg, #000000 0%, #242323 50%, #000000 100%);border-radius: 20px; border: solid gold; width:100%;"
                        src="public/assets/img/pabrik8.jpeg" alt="">

                </div>



                <h1 style="display: none">cara bakar arang</h1>
                <h1 style="display: none">charcoal briquettes</h1>
                <h1 style="display: none">supplier arang</h1>
                <h1 style="display: none">arang barbeque</h1>
                <h1 style="display: none">coconut charcoal briquettes</h1>
                <h1 style="display: none">coconut shell briquettes</h1>

            </div>
        </div>
    </section><!-- #featured-services -->

    <!--==========================
      Call To Action Section
    ============================-->
    <!-- <section id="call-to-action" class="wow fadeIn">
        <div class="container text-center">
            <h3>Call To Action</h3>
            <p> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            <a class="cta-btn" href="#">Call To Action</a>
        </div>
    </section>#call-to-action -->

    <!--==========================
      Skills Section
    ============================-->
    <!-- <section id="skills">
        <div class="container">

            <header class="section-header">
                <h3>Our Skills</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
            </header>

            <div class="skills-content">

                <div class="progress">
                    <div class="progress-bar bg-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                        <span class="skill">HTML <i class="val">100%</i></span>
                    </div>
                </div>

                <div class="progress">
                    <div class="progress-bar bg-info" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
                        <span class="skill">CSS <i class="val">90%</i></span>
                    </div>
                </div>

                <div class="progress">
                    <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">
                        <span class="skill">JavaScript <i class="val">75%</i></span>
                    </div>
                </div>

                <div class="progress">
                    <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100">
                        <span class="skill">Photoshop <i class="val">55%</i></span>
                    </div>
                </div>

            </div>

        </div>
    </section> -->

    <!--==========================
      Facts Section
    ============================-->
    <!-- <section id="facts" class="wow fadeIn">
        <div class="container">

            <header class="section-header">
                <h3>Facts</h3>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p>
            </header>

            <div class="row counters">

                <div class="col-lg-3 col-6 text-center">
                    <span data-toggle="counter-up">274</span>
                    <p>Clients</p>
                </div>

                <div class="col-lg-3 col-6 text-center">
                    <span data-toggle="counter-up">421</span>
                    <p>Projects</p>
                </div>

                <div class="col-lg-3 col-6 text-center">
                    <span data-toggle="counter-up">1,364</span>
                    <p>Hours Of Support</p>
                </div>

                <div class="col-lg-3 col-6 text-center">
                    <span data-toggle="counter-up">18</span>
                    <p>Hard Workers</p>
                </div>

            </div>

            <div class="facts-img">
                <img src="img/facts-img.png" alt="" class="img-fluid">
            </div>

        </div>
    </section>#facts -->

    <!--==========================
      Portfolio Section
    ============================-->
    <!-- <section id="portfolio" class="section-bg">
        <div class="container">

            <header class="section-header">
                <h3 class="section-title">Our Portfolio</h3>
            </header>

            <div class="row">
                <div class="col-lg-12">
                    <ul id="portfolio-flters">
                        <li data-filter="*" class="filter-active">All</li>
                        <li data-filter=".filter-app">App</li>
                        <li data-filter=".filter-card">Card</li>
                        <li data-filter=".filter-web">Web</li>
                    </ul>
                </div>
            </div>

            <div class="row portfolio-container">

                <div class="col-lg-4 col-md-6 portfolio-item filter-app wow fadeInUp">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="img/portfolio/app1.jpg" class="img-fluid" alt="">
                            <a href="img/portfolio/app1.jpg" data-lightbox="portfolio" data-title="App 1" class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">App 1</a></h4>
                            <p>App</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.1s">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="img/portfolio/web3.jpg" class="img-fluid" alt="">
                            <a href="img/portfolio/web3.jpg" class="link-preview" data-lightbox="portfolio" data-title="Web 3" title="Preview"><i class="ion ion-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">Web 3</a></h4>
                            <p>Web</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-app wow fadeInUp" data-wow-delay="0.2s">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="img/portfolio/app2.jpg" class="img-fluid" alt="">
                            <a href="img/portfolio/app2.jpg" class="link-preview" data-lightbox="portfolio" data-title="App 2" title="Preview"><i class="ion ion-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">App 2</a></h4>
                            <p>App</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-card wow fadeInUp">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="img/portfolio/card2.jpg" class="img-fluid" alt="">
                            <a href="img/portfolio/card2.jpg" class="link-preview" data-lightbox="portfolio" data-title="Card 2" title="Preview"><i class="ion ion-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">Card 2</a></h4>
                            <p>Card</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.1s">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="img/portfolio/web2.jpg" class="img-fluid" alt="">
                            <a href="img/portfolio/web2.jpg" class="link-preview" data-lightbox="portfolio" data-title="Web 2" title="Preview"><i class="ion ion-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">Web 2</a></h4>
                            <p>Web</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-app wow fadeInUp" data-wow-delay="0.2s">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="img/portfolio/app3.jpg" class="img-fluid" alt="">
                            <a href="img/portfolio/app3.jpg" class="link-preview" data-lightbox="portfolio" data-title="App 3" title="Preview"><i class="ion ion-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">App 3</a></h4>
                            <p>App</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-card wow fadeInUp">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="img/portfolio/card1.jpg" class="img-fluid" alt="">
                            <a href="img/portfolio/card1.jpg" class="link-preview" data-lightbox="portfolio" data-title="Card 1" title="Preview"><i class="ion ion-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">Card 1</a></h4>
                            <p>Card</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-card wow fadeInUp" data-wow-delay="0.1s">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="img/portfolio/card3.jpg" class="img-fluid" alt="">
                            <a href="img/portfolio/card3.jpg" class="link-preview" data-lightbox="portfolio" data-title="Card 3" title="Preview"><i class="ion ion-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">Card 3</a></h4>
                            <p>Card</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.2s">
                    <div class="portfolio-wrap">
                        <figure>
                            <img src="img/portfolio/web1.jpg" class="img-fluid" alt="">
                            <a href="img/portfolio/web1.jpg" class="link-preview" data-lightbox="portfolio" data-title="Web 1" title="Preview"><i class="ion ion-eye"></i></a>
                            <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                        </figure>

                        <div class="portfolio-info">
                            <h4><a href="#">Web 1</a></h4>
                            <p>Web</p>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>#portfolio -->

    <!--==========================
      Clients Section
    ============================-->
    <!-- <section id="clients" class="wow fadeInUp">
        <div class="container">

            <header class="section-header">
                <h3>Our Clients</h3>
            </header>

            <div class="owl-carousel clients-carousel">
                <img src="img/clients/client-1.png" alt="">
                <img src="img/clients/client-2.png" alt="">
                <img src="img/clients/client-3.png" alt="">
                <img src="img/clients/client-4.png" alt="">
                <img src="img/clients/client-5.png" alt="">
                <img src="img/clients/client-6.png" alt="">
                <img src="img/clients/client-7.png" alt="">
                <img src="img/clients/client-8.png" alt="">
            </div>

        </div>
    </section>#clients -->

    <!--==========================
      Clients Section
    ============================-->
    <!-- <section id="testimonials" class="section-bg wow fadeInUp">
        <div class="container">

            <header class="section-header">
                <h3>Testimonials</h3>
            </header>

            <div class="owl-carousel testimonials-carousel">

                <div class="testimonial-item">
                    <img src="img/testimonial-1.jpg" class="testimonial-img" alt="">
                    <h3>Saul Goodman</h3>
                    <h4>Ceo &amp; Founder</h4>
                    <p>
                        <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
                        Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
                        <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
                    </p>
                </div>

                <div class="testimonial-item">
                    <img src="img/testimonial-2.jpg" class="testimonial-img" alt="">
                    <h3>Sara Wilsson</h3>
                    <h4>Designer</h4>
                    <p>
                        <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
                        Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim culpa.
                        <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
                    </p>
                </div>

                <div class="testimonial-item">
                    <img src="img/testimonial-3.jpg" class="testimonial-img" alt="">
                    <h3>Jena Karlis</h3>
                    <h4>Store Owner</h4>
                    <p>
                        <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
                        Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem veniam duis minim tempor labore quem eram duis noster aute amet eram fore quis sint minim.
                        <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
                    </p>
                </div>

                <div class="testimonial-item">
                    <img src="img/testimonial-4.jpg" class="testimonial-img" alt="">
                    <h3>Matt Brandon</h3>
                    <h4>Freelancer</h4>
                    <p>
                        <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
                        Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim fugiat minim velit minim dolor enim duis veniam ipsum anim magna sunt elit fore quem dolore labore illum veniam.
                        <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
                    </p>
                </div>

                <div class="testimonial-item">
                    <img src="img/testimonial-5.jpg" class="testimonial-img" alt="">
                    <h3>John Larson</h3>
                    <h4>Entrepreneur</h4>
                    <p>
                        <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
                        Quis quorum aliqua sint quem legam fore sunt eram irure aliqua veniam tempor noster veniam enim culpa labore duis sunt culpa nulla illum cillum fugiat legam esse veniam culpa fore nisi cillum quid.
                        <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
                    </p>
                </div>

            </div>

        </div>
    </section>#testimonials -->

    <!--==========================
      Team Section
    ============================-->
    <!-- <section id="team">
        <div class="container">
            <div class="section-header wow fadeInUp">
                <h3>Team</h3>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p>
            </div>

            <div class="row">

                <div class="col-lg-3 col-md-6 wow fadeInUp">
                    <div class="member">
                        <img src="img/team-1.jpg" class="img-fluid" alt="">
                        <div class="member-info">
                            <div class="member-info-content">
                                <h4>Walter White</h4>
                                <span>Chief Executive Officer</span>
                                <div class="social">
                                    <a href=""><i class="fa fa-twitter"></i></a>
                                    <a href=""><i class="fa fa-facebook"></i></a>
                                    <a href=""><i class="fa fa-google-plus"></i></a>
                                    <a href=""><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="member">
                        <img src="img/team-2.jpg" class="img-fluid" alt="">
                        <div class="member-info">
                            <div class="member-info-content">
                                <h4>Sarah Jhonson</h4>
                                <span>Product Manager</span>
                                <div class="social">
                                    <a href=""><i class="fa fa-twitter"></i></a>
                                    <a href=""><i class="fa fa-facebook"></i></a>
                                    <a href=""><i class="fa fa-google-plus"></i></a>
                                    <a href=""><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                    <div class="member">
                        <img src="img/team-3.jpg" class="img-fluid" alt="">
                        <div class="member-info">
                            <div class="member-info-content">
                                <h4>William Anderson</h4>
                                <span>CTO</span>
                                <div class="social">
                                    <a href=""><i class="fa fa-twitter"></i></a>
                                    <a href=""><i class="fa fa-facebook"></i></a>
                                    <a href=""><i class="fa fa-google-plus"></i></a>
                                    <a href=""><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                    <div class="member">
                        <img src="img/team-4.jpg" class="img-fluid" alt="">
                        <div class="member-info">
                            <div class="member-info-content">
                                <h4>Amanda Jepson</h4>
                                <span>Accountant</span>
                                <div class="social">
                                    <a href=""><i class="fa fa-twitter"></i></a>
                                    <a href=""><i class="fa fa-facebook"></i></a>
                                    <a href=""><i class="fa fa-google-plus"></i></a>
                                    <a href=""><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>#team -->

    <!--==========================
      Contact Section
    ============================-->
    <!-- <section id="contact" class="section-bg wow fadeInUp">
        <div class="container">

            <div class="section-header">
                <h3>Contact Us</h3>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque</p>
            </div>

            <div class="row contact-info">

                <div class="col-md-4">
                    <div class="contact-address">
                        <i class="ion-ios-location-outline"></i>
                        <h3>Address</h3>
                        <address>A108 Adam Street, NY 535022, USA</address>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="contact-phone">
                        <i class="ion-ios-telephone-outline"></i>
                        <h3>Phone Number</h3>
                        <p><a href="tel:+155895548855">+1 5589 55488 55</a></p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="contact-email">
                        <i class="ion-ios-email-outline"></i>
                        <h3>Email</h3>
                        <p><a href="mailto:info@example.com">info@example.com</a></p>
                    </div>
                </div>

            </div>

            <div class="form">
                <div id="sendmessage">Your message has been sent. Thank you!</div>
                <div id="errormessage"></div>
                <form action="" method="post" role="form" class="contactForm">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                            <div class="validation"></div>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                            <div class="validation"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                        <div class="validation"></div>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                        <div class="validation"></div>
                    </div>
                    <div class="text-center"><button type="submit">Send Message</button></div>
                </form>
            </div>

        </div>
    </section>#contact -->
</main>

<?= $this->endSection(); ?>
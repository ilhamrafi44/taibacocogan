<?= $this->extend('template/layout'); ?>
<?= $this->section('content'); ?>
<style>
    .dotted {
        border: 6px dotted #ffffff;
        border-style: none none dotted;
        color: #fff;
    }
</style>
<section id="intro" style="height: auto;">

    <img src="public/assets/img/news.jpg" alt="" style="width: 100%;">
    <!-- <h1 style="font-weight: bolder;">COMPANY</h1> -->

</section><!-- #intro -->

<main id="main">

    <!--==========================
      Featured Services Section
    ============================-->
    <section id="featured-services">
        <div class="container">
            <div class="row">

                <div class="col-lg-12 text-center">

                    <img src="public/assets/img/ .png" alt="" style="width: 100%;">

                </div>


            </div>
        </div>
    </section><!-- #featured-services -->

    <!--==========================
      Featured Services Section
    ============================-->
    <section id="featured-services">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-5 box text-center">
                    <h4 class="title text-right"><a href="">Briket Arang Batok Kelapa 19 Kontainer Diekspor ke Tiga Negara</a></h4>
                    <style>
                        .dotted {
                            border: 6px dotted #ffffff;
                            border-style: none none dotted;
                            color: #fff;
                        }
                    </style>
                    <hr class='dotted' />
                </div>

                <div class="col-lg-5 box text-center">
                </div>

                <div class="col-lg-2 box text-center">
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 box text-center">
                    <div>
                        <img style="background-image: linear-gradient(
0deg
, #000000 0%, #242323 50%, #000000 100%);
border-radius: 20px;
border: solid gold; width:100%;" src="public/assets/img/pabrik10.jpeg" alt="">
                    </div>
                </div>
                <div class="col-lg-6 box text-center">
                    <p class="text-justify">

                        Bisnis.com, BANGKALAN - Kementerian Pertanian Republik Indonesia melalui Karantina Pertanian Bangkalan, Jawa Timur, Senin (10/8/2020), memberangkatkan sebanyak 19 kontainer briket arang batok kelapa untuk diekspor ke tiga negara, yakni Rusia, Ukraina dan Moldova. Menurut Kepala Karantina Pertanian Bangkalan Agus Mugiyanto, komoditas ekspor berupa briket arang batok kelapa sebanyak 19 kontainer atau senilai Rp2,7 miliar itu diberangkatkan setelah melalui serangkaian tindakan pemeriksaan karantina sesuai dengan persyaratan negara tujuan. "Ini merupakan pemberangkatan ekspor briket arang batok kelapa kali kedua di tengah pandemi Covid-19 kali ini," kata Agus Mugiyanto dalam keterangan tertulis yang disampaikan kepada Antara di Bangkalan, Senin. Menurut Agus, briket arang tempurung kelapa atau choconut charcoal merupakan produk olahan dari pohon kelapa. Potensi ekspor ini dikembangkan sehingga dapat meningkatkan nilai dari tempurung kelapa.

                    </p>
                    <br><br>
                    <small>Sumber : <a href="https://surabaya.bisnis.com/read/20200810/532/1277407/briket-arang-batok-kelapa-19-kontainer-diekspor-ke-tiga-negara">https://surabaya.bisnis.com/read/20200810/532/1277407/briket-arang-batok-kelapa-19-kontainer-diekspor-ke-tiga-negara</a></small>
                </div>
            </div>
        </div>
    </section>

    <section id="featured-services">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-5 box text-center">
                    <h4 class="title text-right"><a href="">
                            Bisnis Arang Batok Kelapa Tembus Pasar Ekspor</a></h4>
                    <style>
                        .dotted {
                            border: 6px dotted #ffffff;
                            border-style: none none dotted;
                            color: #fff;
                        }
                    </style>
                    <hr class='dotted' />
                </div>

                <div class="col-lg-5 box text-center">
                </div>

                <div class="col-lg-2 box text-center">
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 box text-center">
                    <div>
                        <img style="background-image: linear-gradient(
0deg
, #000000 0%, #242323 50%, #000000 100%);
border-radius: 20px;
border: solid gold; width:100%;" src="public/assets/img/pabrik8.jpeg" alt="">
                    </div>
                </div>
                <div class="col-lg-6 box text-center">
                    <p class="text-justify">
                        JAKARTA - Pemerintah mendorong produk UMKM Indonesia agar tembus pasar ekspor. Pelaku UMKM asal Kendal Istikanah berbagi cerita produk arang dari batok kelapa produksinya menembus pasar ekspor.
                        Saat sempat berbincang dengan Menteri Keuangan Sri Mulyani dalam sebuah pameran kerja sama Kemenkeu dan Pemerintah Kabupaten Kendal, Istikanah yang sudah berbisnis briket arang batok sejak 9 tahun lalu itu menyampaikan terima kasihnya pada pemerintah.

                        Istikanah mendapat pendampingan, pelatihan, dan akses pembiayaan ekspor dari LPEI sebagai Special Mission Vehicle Kementerian Keuangan RI. Kini, Istikanah kini berhasil melakukan ekspor ke berbagai negara, termasuk ke Timur Tengah.
                        “Kami punya produk briket arang kelapa dengan kualitas terbaik di dunia, hal ini berkat dukungan program LPEI, yang sangat membantu saya dalam mengembangkan usaha,” ujar Istikanah, yang juga Direktur CV Indoarab Interprise, Selasa (6/4/2021).

                        Dia menuturkan, sebelum mendapat pendampingan dari pemerintah, dia cukup kesulitan untuk melakukan ekspor. Kemudian ketika CV Indoarab Interprise mengikuti program Coaching Program for New Exporter (CPNE) dari LPEI pada tahun 2020, Istikanah mengakui adanya peningkatan kinerja ekspornya. Selain itu, CV Indoarab Interprise juga mendapatkan Fasilitas Pembiayaan Penugasan Khusus Ekspor (PKE) senilai Rp1,5 Miliar yang mampu mendorong kenaikan ekspor hingga 7 ton per bulan.
                    </p>
                    <br><br>
                    <small>Sumber : <a href="https://economy.okezone.com/read/2021/04/06/455/2390189/bisnis-arang-batok-kelapa-tembus-pasar-ekspor">https://economy.okezone.com/read/2021/04/06/455/2390189/bisnis-arang-batok-kelapa-tembus-pasar-ekspor</a></small>
                </div>
            </div>
        </div>
    </section>

    <section id="featured-services">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-5 box text-center">
                    <h4 class="title text-right"><a href="">Potensi Ekspor Besar, Kelangkaan Bahan Baku Briket Arang Kelapa Jadi Kendala</a></h4>
                    <style>
                        .dotted {
                            border: 6px dotted #ffffff;
                            border-style: none none dotted;
                            color: #fff;
                        }
                    </style>
                    <hr class='dotted' />
                </div>

                <div class="col-lg-5 box text-center">
                </div>

                <div class="col-lg-2 box text-center">
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 box text-center">
                    <div>
                        <img style="background-image: linear-gradient(
0deg
, #000000 0%, #242323 50%, #000000 100%);
border-radius: 20px;
border: solid gold; width:100%;" src="public/assets/img/pabrik6.jpeg" alt="">
                    </div>
                </div>
                <div class="col-lg-6 box text-center">
                    <p class="text-justify">
                        SEMARANGTENGAH, AYOSEMARANG.COM -- Meski potensi besar dalam ekspor, Himpunan Pengusaha Briket Arang Kelapa Indonesia (HIPBAKI), menilai masih ada sejumlah kendala yang dihadapi para pengusaha saat ini.

                        Ketua HIPBAKI, Basuki mengatakan, salah satu persoalan yang dihadapi para pengusaha Briket ini adalah kelangkaan bahan baku briket.

                        "Ini menjadi hambatan yang sangat utama, karena kelapa butir utuh di ekspor secara berlebihan. Sehingga menyebabkan tempurung kelapa yang menjadi bahan dasar briket jadi sulit didapat. Sehingga produksi briket tak bisa sesui permintaan," ujarnya, Sabtu (28/11/2020).

                        Saat tidak beroperasional karena adanya kelangkaan bahan dasar briket, otomatis membuat tenaga kerja mengalami penurunan pendapatan bahkan cenderung ada yang dirumahkan sehingga berdampak pada pengangguran.

                        "Belum adanya sinkronisasi antar asosiasi, atau himpunan terkait komoditas usaha kelapa dan turunannya berakibat hanya penyelesaian secara sektoral atau sesuai kepentingan masing-masing," imbuhnya.

                        Tak hanya itu, pengiriman produk briket arang kelapa untuk ekspor sangat terkendala pengiriman. Pihaknya pun masih bertanya-tanya lantaran beberapa pihak shipping enggan mengangkut produksi briket. Sehingga terjadi penumpukan produk briket di gudang pabrik masing-masing anggota.

                        "Kami berharap pemerintah hadir dalam regulasinya untuk penstabilan bahan baku briket tempurung kelapa ini pada khususnya dan usaha turunan kelapa pada umumnya. Sehingga normalisasi usaha produksi briket bisa terjamin," katanya.

                        "Selain itu, kami juga berharap Pemerintah bisa membantu dalam koordinasi dengan pihak shipping atau pengangkutan karena sampai saat ini pengiriman ekspor tertunda sampai kapan batas waktu yang kurang jelas," imbuhnya.
                    </p>
                    <br><br>
                    <small>Sumber : <a href="https://m.ayosemarang.com/read/2020/11/28/67818/potensi-ekspor-besar-kelangkaan-bahan-baku-briket-arang-kelapa-jadi-kendala">https://m.ayosemarang.com/read/2020/11/28/67818/potensi-ekspor-besar-kelangkaan-bahan-baku-briket-arang-kelapa-jadi-kendala</a></small>
                </div>
            </div>
        </div>
    </section>
    <!-- #featured-services -->

    <section id="featured-services">
        <div class="container-fluid">

        </div>
    </section>








</main>

<?= $this->endSection(); ?>
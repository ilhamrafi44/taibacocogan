<?= $this->extend('template/layout'); ?>
<?= $this->section('content'); ?>
<style>
.dotted {
    border: 6px dotted #ffffff;
    border-style: none none dotted;
    color: #fff;
}
</style>
<section id="intro" style="height: auto;">

    <img src="public/assets/img/news.jpg" alt="charcoal briquettes" style="width: 100%;">
    <!-- <h1 style="font-weight: bolder;">COMPANY</h1> -->

</section><!-- #intro -->

<main id="main">

    <!--==========================
      Featured Services Section
    ============================-->
    <section id="featured-services">
        <div class="container">
            <div class="row">

                <div class="col-lg-12 text-center">

                    <img src="public/assets/img/ .png" alt="charcoal briquettes" style="width: 100%;">

                </div>


            </div>
        </div>
    </section><!-- #featured-services -->

    <!--==========================
      Featured Services Section
    ============================-->

    <?php foreach ($gambar as $p) : ?>

    <section id="featured-services">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-5 box text-center">
                    <h4 class="title text-right"><a href="#<?= $p['nama_article_id'] ?><?= $p['id'] ?>">
                            <?php if (WEB_LANG == 'id') {
                                    echo lang($p['nama_article_id']);
                                } elseif (WEB_LANG == 'en') {
                                    echo lang($p['nama_article_en']);
                                } else {
                                    echo lang($p['nama_article_ar']);
                                } ?></a></h4>

                    <style>
                    .dotted {
                        border: 6px dotted #ffffff;
                        border-style: none none dotted;
                        color: #fff;
                    }
                    </style>
                    <hr class='dotted' />
                </div>

                <div class="col-lg-5 box text-center">
                </div>
                <h1 style="display: none">cara bakar arang</h1>
                <h1 style="display: none">charcoal briquettes</h1>
                <h1 style="display: none">supplier arang</h1>
                <h1 style="display: none">arang barbeque</h1>
                <h1 style="display: none">coconut charcoal briquettes</h1>
                <h1 style="display: none">coconut shell briquettes</h1>

                <div class="col-lg-2 box text-center">
                </div>
            </div>

            <div class="row" id="<?= $p['nama_article_id']; ?><?= $p['id']; ?>">
                <div class="col-lg-6 box text-center">
                    <div>
                        <img style="background-image: linear-gradient(
0deg
, #000000 0%, #242323 50%, #000000 100%);
border-radius: 20px;
border: solid gold; width:100%;" src="<?= base_url() . "/public/admins/uploads/" . $p['pict_1']; ?>"
                            alt="charcoal briquettes"><br><br>
                        <img style="background-image: linear-gradient(
0deg
, #000000 0%, #242323 50%, #000000 100%);
border-radius: 20px;
border: solid gold; width:100%;" src="<?= base_url() . "/public/admins/uploads/" . $p['pict_2']; ?>"
                            alt="charcoal briquettes">
                    </div>
                </div>
                <div class="col-lg-6 box text-center">
                    <p class="text-justify" style="font-size:20px;">

                        <?php if (WEB_LANG == 'id') {
                                echo lang($p['desc_article_id']);
                            } elseif (WEB_LANG == 'en') {
                                echo lang($p['desc_article_en']);
                            } else {
                                echo lang($p['desc_article_ar']);
                            } ?>
                    </p>
                    <br><br>
                    <!-- <small>Sumber : <a href="https://surabaya.bisnis.com/read/20200810/532/1277407/briket-arang-batok-kelapa-19-kontainer-diekspor-ke-tiga-negara">https://surabaya.bisnis.com/read/20200810/532/1277407/briket-arang-batok-kelapa-19-kontainer-diekspor-ke-tiga-negara</a></small> -->
                </div>
            </div>
        </div>
    </section>
    <?php endforeach; ?>
    <!-- #featured-services -->

    <section id="featured-services">
        <div class="container-fluid">

        </div>
    </section>






    <h1 style="display: none">cara bakar arang</h1>
    <h1 style="display: none">charcoal briquettes</h1>
    <h1 style="display: none">supplier arang</h1>
    <h1 style="display: none">arang barbeque</h1>
    <h1 style="display: none">coconut charcoal briquettes</h1>
    <h1 style="display: none">coconut shell briquettes</h1>

</main>

<?= $this->endSection(); ?>
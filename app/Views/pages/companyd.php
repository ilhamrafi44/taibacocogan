<?= $this->extend('template/layout'); ?>
<?= $this->section('content'); ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">
<style>
    .dotted {
        border: 6px dotted #ffffff;
        border-style: none none dotted;
        color: #fff;
    }

    .photo-gallery {
        color: #313437;
        /* background-color: #fff; */
    }

    .photo-gallery p {
        color: #7d8285;
    }

    .photo-gallery h2 {
        font-weight: bold;
        margin-bottom: 40px;
        padding-top: 40px;
        color: #ffffff;
    }

    @media (max-width:767px) {
        .photo-gallery h2 {
            margin-bottom: 25px;
            padding-top: 25px;
            font-size: 24px;
        }
    }

    .photo-gallery .intro {
        font-size: 16px;
        max-width: 500px;
        margin: 0 auto 40px;
    }

    .photo-gallery .intro p {
        margin-bottom: 0;
    }

    .photo-gallery .photos {
        padding-bottom: 20px;
    }

    .photo-gallery .item {
        padding-bottom: 30px;
    }
</style><?php if (WEB_LANG == 'ar') {
            echo '  <style>#featured-services p {
                        font-size: 18px;
                      
                        color: #fff;}

                  
                     </style>';
        }  ?>
<section id="intro" style="height: auto;">

    <img src="public/assets/img/companybgg.png" alt="" style="width: 100%;">
    <!-- <h1 style="font-weight: bolder;">COMPANY</h1> -->

</section><!-- #intro -->

<main id="main">

    <!--==========================
      Featured Services Section
    ============================-->



    <section id="featured-services">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-5 box text-center">
                    <h4 class="title text-right"><a href="">Achievment & Certificate</a></h4>
                    <style>
                        .dotted {
                            border: 6px dotted #ffffff;
                            border-style: none none dotted;
                            color: #fff;
                        }
                    </style>
                    <hr class='dotted' />

                </div>

                <div class="col-lg-5 box text-center">

                </div>

                <div class="col-lg-2 box text-center">
                </div>

            </div>


            <div class="photo-gallery">
                <div class="container">
                    <div class="intro">
                        <!-- <h2 class="text-center">Lightbox Gallery</h2>
                        <p class="text-center">Nunc luctus in metus eget fringilla. Aliquam sed justo ligula. Vestibulum nibh erat, pellentesque ut laoreet vitae. </p> -->
                    </div>
                    <div class="row photos">

                        <div class="col-sm-6 col-md-4 col-lg-3 item">
                            <a href="public/assets/img/ach/1.png" data-lightbox="photos"><img style="background-image: linear-gradient(
0deg
, #000000 0%, #242323 50%, #000000 100%);
    border-radius: 20px;
    border: solid gold; width:100%;" class="img-fluid" src="public/assets/img/ach/1.png"></a><br>
                            <p style="padding-top: 10px;" class="text-center">
                                xRoA
                            </p>
                        </div>

                        <div class="col-sm-6 col-md-4 col-lg-3 item">
                            <a href="public/assets/img/ach/2.png" data-lightbox="photos"><img style="background-image: linear-gradient(
0deg
, #000000 0%, #242323 50%, #000000 100%);
    border-radius: 20px;
    border: solid gold; width:100%;" class="img-fluid" src="public/assets/img/ach/2.png"></a><br>
                            <p style="padding-top: 10px;" class="text-center">
                                xMSDS
                            </p>
                        </div>

                        <div class="col-sm-6 col-md-4 col-lg-3 item">
                            <a href="public/assets/img/ach/3.png" data-lightbox="photos"><img style="background-image: linear-gradient(
0deg
, #000000 0%, #242323 50%, #000000 100%);
    border-radius: 20px;
    border: solid gold; width:100%;" class="img-fluid" src="public/assets/img/ach/3.png"></a><br>
                            <p style="padding-top: 10px;" class="text-center">
                                xMSDS
                            </p>
                        </div>

                        <div class="col-sm-6 col-md-4 col-lg-3 item">
                            <a href="public/assets/img/ach/4.png" data-lightbox="photos"><img style="background-image: linear-gradient(
0deg
, #000000 0%, #242323 50%, #000000 100%);
    border-radius: 20px;
    border: solid gold; width:100%;" class="img-fluid" src="public/assets/img/ach/4.png"></a><br>
                            <p style="padding-top: 10px;" class="text-center">
                                xSHT
                            </p>
                        </div>

                        <div class="col-sm-6 col-md-4 col-lg-3 item">
                            <a href="public/assets/img/ach/5.png" data-lightbox="photos"><img style="background-image: linear-gradient(
0deg
, #000000 0%, #542323 50%, #000000 100%);
    border-radius: 20px;
    border: solid gold; width:100%;" class="img-fluid" src="public/assets/img/ach/5.png"></a><br>
                            <p style="padding-top: 10px;" class="text-center">
                                xSHT
                            </p>
                        </div>

                        <div class="col-sm-6 col-md-4 col-lg-3 item">
                            <a href="public/assets/img/ach/6.jpeg" data-lightbox="photos"><img style="background-image: linear-gradient(
0deg
, #000000 0%, #542323 50%, #000000 100%);
    border-radius: 20px;
    border: solid gold; width:100%;" class="img-fluid" src="public/assets/img/ach/6.jpeg"></a><br>
                            <p style="padding-top: 10px;" class="text-center">
                                xSHT
                            </p>
                        </div>




                    </div>
                </div>
            </div>


        </div>
    </section><!-- #featured-services -->





</main>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<?= $this->endSection(); ?>
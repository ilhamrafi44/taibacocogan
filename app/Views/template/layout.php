<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>
        Taibacococha Indonesia &#8211; Taibacococha charcoal briquettes
        &#8211; arang barbeque &#8211; coconut shell briquettes &#8211; supplier arang &#8211; cara bakar arang
    </title>
    <meta name="keywords"
        content="arang barbeque, charcoal briquettes, supplier briket, coconut charcoal briquettes, coconut shell briquettes, cara bakar arang, media arang" />
    <meta name="description" content="Taibacococha Indonesia" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="manifest" href="site.webmanifest" />

    <!-- Favicons -->
    <link href="public/assets/img/logo.png" rel="icon">
    <link href="public/assets/img/logo.png" rel="apple-touch-icon">
    <link rel="shortcut icon" href="public/assets/img/logo.png" />


    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700"
        rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link href="public/assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="public/assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="public/assets/lib/animate/animate.min.css" rel="stylesheet">
    <link href="public/assets/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="public/assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="public/assets/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="public/assets/css/style.css" rel="stylesheet">

    <!-- =======================================================
    Theme Name: BizPage
    Theme URL: https://bootstrapmade.com/bizpage-bootstrap-business-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>

    <!--==========================
    Header
  ============================-->
    <header id="header">
        <div class="container-fluid">

            <div id="logo" class="pull-left">
                <!-- <h1><a href="#intro" class="scrollto">Taibacoco</a></h1> -->
                <!-- Uncomment below if you prefer to use an image logo -->
                <a href="#intro" class="scrollto"><img src="public/assets/img/testt.png" alt="" title="" /></a>
            </div>

            <nav id="nav-menu-container">

                <ul class="nav-menu">
                    <li><a href="<?php base_url(); ?>home"><?= lang('Global.home'); ?></a></li>
                    <!-- <li><a href="<?php base_url(); ?>producta">Product</a></li> -->
                    <li class="menu-has-children"><a
                            href="<?php base_url(); ?>company"><?= lang('Global.company'); ?></a>
                        <ul>
                            <li><a href="<?php base_url(); ?>company"><?= lang('Global.presiden'); ?></a></li>
                            <li><a href="<?php base_url(); ?>companyb"><?= lang('Global.tentang'); ?></a></li>
                            <li><a href="<?php base_url(); ?>companyc"><?= lang('Global.mitra'); ?></a></li>
                            <li><a href="<?php base_url(); ?>companyd"><?= lang('Global.sertif'); ?></a></li>
                            <h1 style="display: none">cara bakar arang</h1>
                            <h1 style="display: none">charcoal briquettes</h1>
                            <h1 style="display: none">supplier arang</h1>
                            <h1 style="display: none">arang barbeque</h1>
                            <h1 style="display: none">coconut charcoal briquettes</h1>
                            <h1 style="display: none">coconut shell briquettes</h1>


                            <!-- <li><a href="#">Drop Down 4</a></li>
                            <li><a href="#">Drop Down 5</a></li> -->
                        </ul>
                    </li>
                    <li class="menu-has-children"><a href=""><?= lang('Global.product'); ?></a>
                        <ul>
                            <li><a href="<?php base_url(); ?>producta"><?= lang('Global.sisha'); ?></a></li>
                            <li><a href="<?php base_url(); ?>productb"><?= lang('Global.bbq'); ?></a></li>
                            <li><a href="<?php base_url(); ?>productc"><?= lang('Global.charcoal'); ?></a></li>
                            <li><a href="<?php base_url(); ?>productd"><?= lang('Global.vco'); ?></a></li>
                            <li><a href="<?php base_url(); ?>producte"><?= lang('Global.carbon'); ?></a></li>
                            <li><a href="<?php base_url(); ?>productf"><?= lang('Global.copra'); ?></a></li>
                            <h1 style="display: none">cara bakar arang</h1>
                            <h1 style="display: none">charcoal briquettes</h1>
                            <h1 style="display: none">supplier arang</h1>
                            <h1 style="display: none">arang barbeque</h1>
                            <h1 style="display: none">coconut charcoal briquettes</h1>
                            <h1 style="display: none">coconut shell briquettes</h1>

                            <!-- <li><a href="#">Drop Down 4</a></li>
                            <li><a href="#">Drop Down 5</a></li> -->
                        </ul>
                    </li>
                    <li class="menu-has-children"><a href=""><?= lang('Global.news'); ?></a>
                        <ul>
                            <li><a href="<?php base_url(); ?>article"><?= lang('Global.article'); ?></a></li>
                            <li><a href="<?php base_url(); ?>news"><?= lang('Global.newss'); ?></a></li>
                            <!-- <li><a href="#">Drop Down 4</a></li>
                            <li><a href="#">Drop Down 5</a></li> -->
                        </ul>
                    </li>
                    <!-- <li><a href="#portfolio">Gallery</a></li> -->
                    <li class="menu-has-children"><a href=""><?= lang('Global.gallery'); ?></a>
                        <ul>
                            <li><a href="<?php base_url(); ?>foto"><?= lang('Global.photo'); ?></a></li>
                            <li><a href="<?php base_url(); ?>video"><?= lang('Global.video'); ?></a></li>
                            <!-- <li><a href="#">Drop Down 4</a></li>
                            <li><a href="#">Drop Down 5</a></li> -->
                        </ul>
                    </li>
                    <li><a href="<?php base_url(); ?>contactus"><?= lang('Global.contact'); ?></a></li>

                </ul>

            </nav>
            <ul class="nav-menu">
                <li class="menu-has-children" style="margin-left:190px; bottom:4px;"><a href="frontlang/en">EN<img
                            style="width:30px;" src="public/assets/img/english.png" alt=""></a>
                    <ul style="">
                        <li style="right: 12px; background-color:transparent; border:none;">

                            <a href="frontlang/id">ID<img style="width:30px;" src="public/assets/img/indo.png"
                                    alt=""></a>
                        </li>
                        <li style="right: 12px; background-color:transparent; border:none;"><a
                                href="frontlang/ar">AR<img style="width:30px;" src="public/assets/img/arabic.png"
                                    alt=""></a></li>

                        <!-- <li><a href="#">Drop Down 4</a></li>
                                <li><a href="#">Drop Down 5</a></li> -->
                    </ul>
                </li>
            </ul>
            <!-- #nav-menu-container -->
        </div>
    </header><!-- #header -->

    <!--==========================
    Intro Section
  ============================-->
    <?= $this->renderSection('content'); ?>
    <!--==========================
    Footer
  ============================-->
    <style>
    .float {
        position: fixed;
        width: 60px;
        height: 60px;
        bottom: 40px;
        right: 40px;
        background-color: #25d366;
        color: #FFF;
        border-radius: 50px;
        text-align: center;
        font-size: 30px;
        box-shadow: 2px 2px 3px #999;
        z-index: 100;
    }

    .my-float {
        margin-top: 16px;
    }
    </style>
    <!--<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe" crossorigin="anonymous"></script>-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <a href="https://api.whatsapp.com/send?phone=6281316958461&text=Halo%20Saya%20Ingin%20Bertanya%20Mengenai%20Produk%20Taibacoco%20."
        class="float" target="_blank">
        <i class="fa fa-whatsapp my-float"></i>
    </a>
    <footer id="footer">
        <h1 style="display: none">cara bakar arang</h1>
        <h1 style="display: none">charcoal briquettes</h1>
        <h1 style="display: none">supplier arang</h1>
        <h1 style="display: none">arang barbeque</h1>
        <h1 style="display: none">coconut charcoal briquettes</h1>
        <h1 style="display: none">coconut shell briquettes</h1>
        <div class="footer-top">
            <div class="container">
                <div class="row">

                    <div class="col col-sm-4 col-lg-4 col-md-6">
                        <img src="public/assets/img/footerlogo.png" alt=""><br><br>
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3964.289543598629!2d106.75012969970703!3d-6.484969139099121!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69c29524f2657b%3A0x76b491e9526d594f!2sJl.%20PWRI%20No.53%2C%20RT.1%2FRW.6%2C%20Tonjong%2C%20Tajur%20Halang%2C%20Bogor%2C%20Jawa%20Barat%2016320!5e0!3m2!1sen!2sid!4v1619441117796!5m2!1sen!2sid"
                            width="300" height="200" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>

                    <div class="col col-sm-4 col-lg-4 col-md-6"
                        style="border-left: 1px solid grey; border-right:solid grey 1px ;">
                        <h4>HEAD OFFICE</h4>
                        <p style="color: #000000;">PT. Taiba Cococha Indonesia
                            JL. Angsana Raya Blok O no 1 Keluarahan Pejaten Timur Kec. Pasar Minggu - Jakarta Selatan
                            12510
                        </p>
                        <p style="color: #ffffff;">Phone : 021 - 22790271</p>
                        <p style="color: #000000;">Email :admin@taibacoco.co.id</p>


                    </div>
                    <style>

                    </style>
                    <div class="col col-sm-4 col-lg-4 col-md-6 footer-fact">
                        <h4>FACTORY</h4>
                        <p style="color: #000000;">PT TAIBA COCOCHA INDONESIA
                            Jl. PWRI No.53, RT.1/RW.6, Tonjong, Tajur Halang, Bogor, Jawa Barat 16320, Indonesia
                        </p>
                        <p style="color: #ffffff;">Phone : 021 - 22790271</p>
                        <p style="color: #000000;">Email :admin@taibacoco.co.id</p>


                        <!-- <div class="social-links">
                            <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                            <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                            <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                        </div> -->

                    </div>

                    <!-- <div class="col-lg-4 col-md-6 footer-newsletter">
                        <h4>Our Newsletter</h4>
                        <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna veniam enim veniam illum dolore legam minim quorum culpa amet magna export quem marada parida nodela caramase seza.</p>
                        <form action="" method="post">
                            <input type="email" name="email"><input type="submit" value="Subscribe">
                        </form>
                    </div> -->

                </div>
            </div>
        </div>

        <!-- <div class="container">
            <div class="copyright">
                &copy; Copyright <strong>BizPage</strong>. All Rights Reserved
            </div>
            <div class="credits">

          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=BizPage

                Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
            </div>
        </div>
    </footer> -->

        <!-- <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a> -->
        <!-- Uncomment below i you want to use a preloader -->
        <div id="preloader"></div>

        <!-- JavaScript Libraries -->
        <script src="public/assets/lib/jquery/jquery.min.js"></script>
        <script src="public/assets/lib/jquery/jquery-migrate.min.js"></script>
        <script src="public/assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="public/assets/lib/easing/easing.min.js"></script>
        <script src="public/assets/lib/superfish/hoverIntent.js"></script>
        <script src="public/assets/lib/superfish/superfish.min.js"></script>
        <script src="public/assets/lib/wow/wow.min.js"></script>
        <script src="public/assets/lib/waypoints/waypoints.min.js"></script>
        <script src="public/assets/lib/counterup/counterup.min.js"></script>
        <script src="public/assets/lib/owlcarousel/owl.carousel.min.js"></script>
        <script src="public/assets/lib/isotope/isotope.pkgd.min.js"></script>
        <script src="public/assets/lib/lightbox/js/lightbox.min.js"></script>
        <script src="public/assets/lib/touchSwipe/jquery.touchSwipe.min.js"></script>
        <!-- Contact Form JavaScript File -->
        <script src="public/assets/contactform/contactform.js"></script>

        <!-- Template Main Javascript File -->
        <script src="public/assets/js/main.js"></script>

</body>

</html>
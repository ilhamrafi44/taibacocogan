<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->add('/adminlang/(:any)', 'SelectLanguage::adminlang');
$routes->add('/frontlang/(:any)', 'SelectLanguage::frontlang');
$routes->get('/', 'Home::index');
$routes->get('/company', 'Home::company');
$routes->get('/companyb', 'Home::companyb');
$routes->get('/companyc', 'Home::companyc');
$routes->get('/companyd', 'Home::companyd');


$routes->get('/producta', 'Home::producta');
$routes->get('/productb', 'Home::productb');
$routes->get('/productc', 'Home::productc');
$routes->get('/productd', 'Home::productd');
$routes->get('/producte', 'Home::producte');
$routes->get('/productf', 'Home::productf');
$routes->get('/news', 'Home::news');
$routes->get('/article', 'Home::article');
$routes->get('/contactus', 'Home::contactus');
$routes->get('/foto', 'Home::foto');
$routes->get('/video', 'Home::video');
$routes->get('/regstr', 'Register::index');
$routes->get('/login', 'Home::logins');
$routes->get('/homes', 'Home::homes');

$routes->get('/admins', 'Admins::index', ['filter' => 'auth']);
$routes->get('/front', 'Admins::front', ['filter' => 'auth']);
$routes->get('/productas', 'Admins::productas', ['filter' => 'auth']);
$routes->get('/productab', 'Admins::productab', ['filter' => 'auth']);
$routes->get('/productac', 'Admins::productac', ['filter' => 'auth']);
$routes->get('/admins/save', 'Admins::save', ['filter' => 'auth']);
$routes->delete('/admins/delete/(:num)', 'Admins::delete/$1', ['filter' => 'auth']);
$routes->get('/galleryfoto', 'Admins::galleryfoto', ['filter' => 'auth']);
$routes->get('/galleryvideo', 'Admins::galleryvideo', ['filter' => 'auth']);
$routes->get('/mail', 'Admins::mail', ['filter' => 'auth']);
$routes->get('/articles', 'Admins::article', ['filter' => 'auth']);









/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
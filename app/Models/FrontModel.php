<?php

namespace App\Models;

use CodeIgniter\Model;

class FrontModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'front';
	protected $primaryKey           = 'id';
	// protected $returnType           = 'object';
	protected $useTimestamps = true;
	protected $allowedFields        = ['pict_indo', 'pict_inggris', 'pict_arab'];
}

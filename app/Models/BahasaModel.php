<?php

namespace App\Models;

use CodeIgniter\Model;

class BahasaModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'bahasa';
	protected $primaryKey           = 'id';
	// protected $returnType           = 'object';
	protected $useTimestamps = true;
	protected $allowedFields        = [
		'category_product', 'nama_indo', 'nama_inggris', 'nama_arab', 'desc_indo', 'desc_inggris', 'desc_arab', 'ket_indo',
		'ket_inggris', 'ket_arab', 'pict'
	];

	public function __construct()
	{
		parent::__construct();
		$this->db = \Config\Database::connect();
	}

	public function countVerf()
	{
		$db = $this->db = \Config\Database::connect();
		$query = $db->query('SELECT * FROM bahasa');
		return $query->getNumRows();
	}
}

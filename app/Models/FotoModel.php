<?php

namespace App\Models;

use CodeIgniter\Model;

class FotoModel extends Model
{
    protected $DBGroup              = 'default';
    protected $table                = 'gfoto';
    protected $primaryKey           = 'id';
    // protected $returnType           = 'object';
    protected $useTimestamps = true;
    protected $allowedFields        = [
        'pict', 'desc_indo', 'desc_inggris', 'desc_arab'
    ];
}

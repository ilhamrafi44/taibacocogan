<?php

namespace App\Models;

use CodeIgniter\Model;

class VideoModel extends Model
{
    protected $DBGroup              = 'default';
    protected $table                = 'gvideo';
    protected $primaryKey           = 'id';
    // protected $returnType           = 'object';
    protected $useTimestamps = true;
    protected $allowedFields        = [
        'pict', 'link', 'desc_indo', 'desc_inggris', 'desc_arab'
    ];
}

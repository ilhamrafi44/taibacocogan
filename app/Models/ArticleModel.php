<?php

namespace App\Models;

use CodeIgniter\Model;

class ArticleModel extends Model
{
    protected $DBGroup              = 'default';
    protected $table                = 'article';
    protected $primaryKey           = 'id';
    // protected $returnType           = 'object';
    protected $useTimestamps = true;
    protected $allowedFields        = [
        'nama_article_id','nama_article_en', 'nama_article_ar', 'desc_article_id','desc_article_en','desc_article_ar', 'pict_1', 'pict_2'
    ];

    public function __construct()
    {
        parent::__construct();
        $this->db = \Config\Database::connect();
    }

    public function countVerf()
    {
        $db = $this->db = \Config\Database::connect();
        $query = $db->query('SELECT * FROM article');
        return $query->getNumRows();
    }
}

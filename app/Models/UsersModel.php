<?php

namespace App\Models;

use CodeIgniter\Model;

class UsersModel extends Model
{
    protected $DBGroup              = 'default';
    protected $table                = 'users';
    protected $primaryKey           = 'user_id';
    // protected $returnType           = 'object';
    protected $allowedFields        = [
        'user_name', 'user_email', 'user_password', 'user_created_at'
    ];
}

<?php

namespace App\Models;

use CodeIgniter\Model;

class FormModel extends Model
{
    protected $DBGroup              = 'default';
    protected $table                = 'form';
    protected $primaryKey           = 'id';
    // protected $returnType           = 'object';
    protected $useTimestamps = true;
    protected $allowedFields        = [
        'name', 'email', 'subject', 'message'
    ];

    public function __construct()
    {
        parent::__construct();
        $this->db = \Config\Database::connect();
    }

    public function countVerf()
    {
        $db = $this->db = \Config\Database::connect();
        $query = $db->query('SELECT * FROM form');
        return $query->getNumRows();
    }
}

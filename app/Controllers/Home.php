<?php

namespace App\Controllers;

use App\Models\UsersModel;

class Home extends BaseController
{

	public function __construct()
	{
		$this->language = \Config\Services::language();
		$this->language->setLocale(WEB_LANG);
	}

	public function index()
	{

		$data = [
			'title' => 'Taibacoco | Homepage',
			'gambar' => $this->frontModel->findAll(),
			'gambare' => $this->frontModel->first(),
		];

		return view('pages/index', $data);
	}

	public function company()
	{
		$data = [
			'title' => 'Taibacoco | Company'
		];

		return view('pages/company', $data);
	}

	public function companyb()
	{
		$data = [
			'title' => 'Taibacoco | Company'
		];

		return view('pages/companyb', $data);
	}


	public function companyc()
	{
		$data = [
			'title' => 'Taibacoco | Company'
		];

		return view('pages/companyc', $data);
	}

	public function companyd()
	{
		$data = [
			'title' => 'Taibacoco | Company'
		];

		return view('pages/companyd', $data);
	}

	public function news()
	{
		$data = [
			'title' => 'Taibacoco | Company'
		];

		return view('pages/news', $data);
	}

	public function article()
	{
		$data = [
			'title' => 'Taibacoco | Company',
			'gambar' => $this->articleModel->findAll(),
		];

		return view('pages/article', $data);
	}

	public function productf()
	{
		$data = [
			'title' => 'Taibacoco | Product',
			'bahasah' => $this->bahasaModel->where('category_product', 'copra')->findAll(),
		];

		return view('pages/productf', $data);
	}

	public function producta()
	{
		$data = [
			'title' => 'Taibacoco | Product',
			'bahasah' => $this->bahasaModel->where('category_product', 'sisha')->findAll(),
			// 'locale' => service('request')->getLocale()

		];

		return view('pages/producta', $data);
	}

	public function productb()
	{
		$data = [
			'title' => 'Taibacoco | Product',
			'bahasah' => $this->bahasaModel->where('category_product', 'bbq')->findAll(),

		];

		return view('pages/productb', $data);
	}

	public function productc()
	{
		$data = [
			'title' => 'Taibacoco | Product',
			'bahasah' => $this->bahasaModel->where('category_product', 'charcoal')->findAll(),

		];

		return view('pages/productc', $data);
	}

	public function productd()
	{
		$data = [
			'title' => 'Taibacoco | Product',
			'bahasah' => $this->bahasaModel->where('category_product', 'vco')->findAll(),

		];

		return view('pages/productd', $data);
	}

	public function producte()
	{
		$data = [
			'title' => 'Taibacoco | Product',
			'bahasah' => $this->bahasaModel->where('category_product', 'carbon')->findAll(),

		];

		return view('pages/producte', $data);
	}

	public function contactus()
	{
		$data = [
			'title' => 'Taibacoco | Contact Us'
		];

		return view('pages/contact', $data);
	}

	public function foto()
	{
		$data = [
			'title' => 'Taibacoco | Galleri Foto',
			'gambar' => $this->fotoModel->findAll()

		];

		return view('pages/foto', $data);
	}

	public function video()
	{
		$data = [
			'title' => 'Taibacoco | Galleri Video',
			'gambar' => $this->videoModel->findAll(),
		];

		return view('pages/video', $data);
	}

	public function sendEmail()
	{

		$this->formModel->save([
			'name' => $this->request->getVar('name'),
			'email' => $this->request->getVar('email'),
			'subject' => $this->request->getVar('subject'),
			'message' => $this->request->getVar('message'),
		]);

		session()->setFlashdata('berhasil', 'Form Berhasil Dikirim.');

		return redirect()->to('/contactus');
	}

	public function logins()
	{
		helper(['form']);
		return view('pages/logins');
	}

	public function auth()
	{
		$session = session();
		$model = new UsersModel();
		$email = $this->request->getVar('email');
		$password = $this->request->getVar('password');
		$data = $model->where('user_email', $email)->first();
		if ($data) {
			$pass = $data['user_password'];
			$verify_pass = password_verify($password, $pass);
			if ($verify_pass) {
				$ses_data = [
					'user_id'       => $data['user_id'],
					'user_name'     => $data['user_name'],
					'user_email'    => $data['user_email'],
					'logged_in'     => TRUE
				];
				$session->set($ses_data);
				return redirect()->to('/admins');
			} else {
				$session->setFlashdata('msg', 'Wrong Password');
				return redirect()->to('/login');
			}
		} else {
			$session->setFlashdata('msg', 'Email not Found');
			return redirect()->to('/login');
		}
	}

	public function logout()
	{
		$session = session();
		$session->destroy();
		return redirect()->to('/login');
	}
}

<?php

namespace App\Controllers;

use App\Models\FrontModel;
use App\Models\BahasaModel;
use App\Models\FotoModel;
use App\Models\VideoModel;
use App\Models\ArticleModel;
use App\Models\VisitorModel;






class Admins extends BaseController
{

    public function __construct()
    {
        helper(['visitor']);
        $this->model = new VisitorModel();
        $this->language = \Config\Services::language();
        $this->language->setLocale(WEB_LANG);
    }

    public function index()
    {
        $site_statics_today = $this->model->get_site_data_for_today();
        $site_statics_last_week = $this->model->get_site_data_for_last_week();
        $chart_data_today = $this->model->get_chart_data_for_today();
        $chart_data_curr_month = $this->model->get_chart_data_for_month_year();
        $data = [
            'title' => 'Taiba Admin | Dashboard',
            'all' => $this->bahasaModel->countVerf(),
            'alls' => $this->formModel->countVerf(),
            'visits_today' => isset($site_statics_today['visits']) ? $site_statics_today['visits'] : 0,
            'visits_last_week' => isset($site_statics_last_week['visits']) ? $site_statics_last_week['visits'] : 0,
            'chart_data_today' => isset($chart_data_today) ? $chart_data_today : array(),
            'chart_data_curr_month' => isset($chart_data_curr_month) ? $chart_data_curr_month : array(),


        ];

        return view('admins/index', $data);
    }

    function get_chart_data()
    {
        if (isset($_POST)) {
            if (isset($_POST['month']) && strlen($_POST['month']) && isset($_POST['year']) && strlen($_POST['year'])) {
                $month = $_POST['month'];
                $year = $_POST['year'];
                $data = $this->model->get_chart_data_for_month_year($month, $year);

                if ($data !== NULL) {
                    foreach ($data as $value) {
                        echo $value->day . "t" . $value->visits . "n";
                    }
                } else {
                    $timestamp = mktime(0, 0, 0, $month);
                    $label = date("F", $timestamp);
                    echo '<div style="width:600px;position:relative;font-weight:bold;top:100px;margin-left:auto;margin-left:auto;color:red;">No data found for the "' . $label . '-' . $year . '"</div>';
                }
            } else if (isset($_POST['month']) && strlen($_POST['month'])) {
                $month = $_POST['month'];
                $data = $this->model->get_chart_data_for_month_year($month);

                if ($data !== NULL) {
                    foreach ($data as $value) {
                        echo $value->day . "t" . $value->visits . "n";
                    }
                } else {
                    $timestamp = mktime(0, 0, 0, $month);
                    $label = date("F", $timestamp);
                    echo '<div style="width:600px;position:relative;font-weight:bold;top:100px;margin-left:auto;margin-left:auto;color:red;">No data found for the "' . $label . '"</div>';
                }
            } else if (isset($_POST['year']) && strlen($_POST['year'])) {
                $year = $_POST['year'];
                $data = $this->model->get_chart_data_for_month_year(0, $year);
                if ($data !== NULL) {
                    foreach ($data as $value) {
                        echo $value->day . "t" . $value->visits . "n";
                    }
                } else {
                    echo '<div style="width:600px;position:relative;font-weight:bold;top:100px;margin-left:auto;margin-left:auto;color:red;">No data found for the "' . $year . '"</div>';
                }
            } else {
                $data = $this->model->get_chart_data_for_month_year();
                if ($data !== NULL) {
                    foreach ($data as $value) {
                        echo $value->day . "t" . $value->visits . "n";
                    }
                } else {
                    echo '<div style="width:600px;position:relative;font-weight:bold;top:100px;margin-left:auto;margin-left:auto;color:red;">No data found!</div>';
                }
            }
        }
    }

    public function front()
    {
        $data = [
            'title' => 'Taiba Admin | Front Page Edit',
            'gambar' => $this->frontModel->findAll()
        ];

        return view('admins/front', $data);
    }

    public function productas()
    {
        $data = [
            'title' => 'Taiba Admin | Product Page Edit',
            'gambar' => $this->bahasaModel->findAll()
        ];

        return view('admins/productas', $data);
    }

    public function productab()
    {
        $data = [
            'title' => 'Taiba Admin | Product Page Edit',
            'gambar' => $this->bahasaModel->findAll()
        ];

        return view('admins/productab', $data);
    }

    public function productac()
    {
        $data = [
            'title' => 'Taiba Admin | Product Page Edit',
            'gambar' => $this->bahasaModel->findAll()
        ];

        return view('admins/productac', $data);
    }

    public function galleryfoto()
    {
        $data = [
            'title' => 'Taiba Admin | Product Page Edit',
            'gambar' => $this->fotoModel->findAll()
        ];

        return view('admins/foto', $data);
    }

    public function savefoto()
    {


        $pict = new FotoModel();
        $dataBerkas = $this->request->getFile('pict');
        $fileName = $dataBerkas->getRandomName();
        $pict->insert([
            'pict' => $fileName,
            'desc_indo' => $this->request->getPost('desc_indo'),
            'desc_inggris' => $this->request->getPost('desc_inggris'),
            'desc_arab' => $this->request->getPost('desc_arab'),
        ]);
        $dataBerkas->move('public/admins/uploads/', $fileName);
        session()->setFlashdata('success', 'Berkas Berhasil diupload');
        return redirect()->to(base_url('galleryfoto'));
    }

    public function deletefoto($id)
    {
        //cari gambar berdasarkan id
        $front = $this->fotoModel->find($id);
        //hapus gambar
        unlink('public/admins/uploads/' . $front['pict']);
        $this->fotoModel->delete($id);
        session()->setFlashdata('success', 'Data berhasil dihapus.');

        return redirect()->to('/galleryfoto');
    }


    public function galleryvideo()
    {
        $data = [
            'title' => 'Taiba Admin | Product Page Edit',
            'gambar' => $this->videoModel->findAll()
        ];

        return view('admins/video', $data);
    }

    public function savevideo()
    {
        $pict = new VideoModel();
        $dataBerkas = $this->request->getFile('pict');
        $fileName = $dataBerkas->getRandomName();
        $pict->insert([
            'pict' => $fileName,
            'link' => $this->request->getPost('link'),
            'desc_indo' => $this->request->getPost('desc_indo'),
            'desc_inggris' => $this->request->getPost('desc_inggris'),
            'desc_arab' => $this->request->getPost('desc_arab'),
        ]);
        $dataBerkas->move('public/admins/uploads/', $fileName);
        session()->setFlashdata('success', 'Berkas Berhasil diupload');
        return redirect()->to(base_url('galleryvideo'));
    }

    public function saveyt()
    {
        $pict = new VideoModel();

        $pict->insert([

            'link' => $this->request->getPost('link'),
            'desc_indo' => $this->request->getPost('desc_indo'),
            'desc_inggris' => $this->request->getPost('desc_inggris'),
            'desc_arab' => $this->request->getPost('desc_arab'),
        ]);

        session()->setFlashdata('success', 'Berkas Berhasil diupload');
        return redirect()->to(base_url('galleryvideo'));
    }

    public function deletevideo($id)
    {
        //cari gambar berdasarkan id
        $front = $this->videoModel->find($id);
        $picts = $front['pict'];

        if ($picts == NULL) {
            $this->videoModel->delete($id);
            session()->setFlashdata('success', 'Data berhasil dihapus.');
            return redirect()->to('/galleryvideo');
        } else {
            //hapus gambar
            unlink('public/admins/uploads/' . $front['pict']);
            $this->videoModel->delete($id);
            session()->setFlashdata('success', 'Data berhasil dihapus.');
            return redirect()->to('/galleryvideo');
        }
    }

    public function save()
    {
        if (!$this->validate([
            'pict_indo' => [
                'rules' => 'uploaded[pict_indo]|mime_in[pict_indo,image/jpg,image/jpeg,image/gif,image/png]|max_size[pict_indo,2048]',
                'errors' => [
                    'uploaded' => 'Harus Ada File yang diupload',
                    'mime_in' => 'File Extention Harus Berupa jpg,jpeg,gif,png',
                    'max_size' => 'Ukuran File Maksimal 2 MB'
                ]
            ],

            'pict_inggris' => [
                'rules' => 'uploaded[pict_inggris]|mime_in[pict_inggris,image/jpg,image/jpeg,image/gif,image/png]|max_size[pict_inggris,2048]',
                'errors' => [
                    'uploaded' => 'Harus Ada File yang diupload',
                    'mime_in' => 'File Extention Harus Berupa jpg,jpeg,gif,png',
                    'max_size' => 'Ukuran File Maksimal 2 MB'
                ]

            ],

            'pict_arab' => [
                'rules' => 'uploaded[pict_arab]|mime_in[pict_arab,image/jpg,image/jpeg,image/gif,image/png]|max_size[pict_arab,2048]',
                'errors' => [
                    'uploaded' => 'Harus Ada File yang diupload',
                    'mime_in' => 'File Extention Harus Berupa jpg,jpeg,gif,png',
                    'max_size' => 'Ukuran File Maksimal 2 MB'
                ]

            ]
        ])) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back()->withInput();
        }

        $pict = new FrontModel();
        $dataIndo = $this->request->getFile('pict_indo');
        $dataInggris = $this->request->getFile('pict_inggris');
        $dataArab = $this->request->getFile('pict_arab');
        $indoName = $dataIndo->getRandomName();
        $inggrisName = $dataInggris->getRandomName();
        $arabName = $dataArab->getRandomName();

        $pict->insert([
            'pict_indo' => $indoName,
            'pict_inggris' => $inggrisName,
            'pict_arab' => $arabName,
        ]);
        $dataIndo->move('public/admins/uploads/', $indoName);
        $dataInggris->move('public/admins/uploads/', $inggrisName);
        $dataArab->move('public/admins/uploads/', $arabName);

        session()->setFlashdata('success', 'Berkas Berhasil diupload');
        return redirect()->to(base_url('/front'));
    }

    public function delete($id)
    {
        //cari gambar berdasarkan id
        $front = $this->frontModel->find($id);
        //hapus gambar
        unlink('public/admins/uploads/' . $front['pict_indo']);
        unlink('public/admins/uploads/' . $front['pict_inggris']);
        unlink('public/admins/uploads/' . $front['pict_arab']);

        $this->frontModel->delete($id);
        session()->setFlashdata('success', 'Data berhasil dihapus');

        return redirect()->to('/front');
    }

    public function saveps()
    {
        if (!$this->validate([
            'pict' => [
                'rules' => 'uploaded[pict]|mime_in[pict,image/jpg,image/jpeg,image/gif,image/png]|max_size[pict,2048]',
                'errors' => [
                    'uploaded' => 'Harus Ada File yang diupload',
                    'mime_in' => 'File Extention Harus Berupa jpg,jpeg,gif,png',
                    'max_size' => 'Ukuran File Maksimal 2 MB'
                ]

            ]
        ])) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back()->withInput();
        }

        $pict = new BahasaModel();
        $dataBerkas = $this->request->getFile('pict');
        $fileName = $dataBerkas->getRandomName();
        $pict->insert([
            'category_product' => 'sisha',
            'nama_indo' => $this->request->getPost('nama_indo'),
            'nama_inggris' => $this->request->getPost('nama_inggris'),
            'nama_arab' => $this->request->getPost('nama_arab'),
            'desc_indo' => $this->request->getPost('desc_indo'),
            'desc_inggris' => $this->request->getPost('desc_inggris'),
            'desc_arab' => $this->request->getPost('desc_arab'),
            'ket_indo' => $this->request->getPost('ket_indo'),
            'ket_inggris' => $this->request->getPost('ket_inggris'),
            'ket_arab' => $this->request->getPost('ket_arab'),
            'pict' => $fileName
        ]);
        $dataBerkas->move('public/admins/uploads/', $fileName);
        session()->setFlashdata('success', 'Berkas Berhasil diupload');
        return redirect()->to(base_url('productas'));
    }

    public function deletes($id)
    {
        //cari gambar berdasarkan id
        $front = $this->bahasaModel->find($id);
        //hapus gambar
        unlink('public/admins/uploads/' . $front['pict']);
        $this->bahasaModel->delete($id);
        session()->setFlashdata('success', 'Data berhasil dihapus.');

        return redirect()->to('/productab');
    }

    public function savepb()
    {

        $pict = new BahasaModel();
        $dataBerkas = $this->request->getFile('pict');
        $fileName = $dataBerkas->getRandomName();
        $pict->insert([
            'category_product' => $this->request->getPost('category_product'),
            'nama_indo' => $this->request->getPost('nama_indo'),
            'nama_inggris' => $this->request->getPost('nama_inggris'),
            'nama_arab' => $this->request->getPost('nama_arab'),
            'desc_indo' => $this->request->getPost('desc_indo'),
            'desc_inggris' => $this->request->getPost('desc_inggris'),
            'desc_arab' => $this->request->getPost('desc_arab'),
            'ket_indo' => $this->request->getPost('ket_indo'),
            'ket_inggris' => $this->request->getPost('ket_inggris'),
            'ket_arab' => $this->request->getPost('ket_arab'),
            'pict' => $fileName
        ]);
        $dataBerkas->move('public/admins/uploads/', $fileName);
        session()->setFlashdata('success', 'Berkas Berhasil diupload');
        return redirect()->to(base_url('productab'));
    }

    public function deleteb($id)
    {
        //cari gambar berdasarkan id
        $front = $this->bahasaModel->find($id);
        //hapus gambar
        unlink('public/admins/uploads/' . $front['pict']);
        $this->bahasaModel->delete($id);
        session()->setFlashdata('success', 'Data berhasil dihapus.');

        return redirect()->to('/front');
    }

    public function savepc()
    {
        if (!$this->validate([
            'pict' => [
                'rules' => 'uploaded[pict]|mime_in[pict,image/jpg,image/jpeg,image/gif,image/png]|max_size[pict,2048]',
                'errors' => [
                    'uploaded' => 'Harus Ada File yang diupload',
                    'mime_in' => 'File Extention Harus Berupa jpg,jpeg,gif,png',
                    'max_size' => 'Ukuran File Maksimal 2 MB'
                ]

            ]
        ])) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back()->withInput();
        }

        $pict = new BahasaModel();
        $dataBerkas = $this->request->getFile('pict');
        $fileName = $dataBerkas->getRandomName();
        $pict->insert([
            'category_product' => 'charcoal',
            'nama_indo' => $this->request->getPost('nama_indo'),
            'nama_inggris' => $this->request->getPost('nama_inggris'),
            'nama_arab' => $this->request->getPost('nama_arab'),
            'desc_indo' => $this->request->getPost('desc_indo'),
            'desc_inggris' => $this->request->getPost('desc_inggris'),
            'desc_arab' => $this->request->getPost('desc_arab'),
            'ket_indo' => $this->request->getPost('ket_indo'),
            'ket_inggris' => $this->request->getPost('ket_inggris'),
            'ket_arab' => $this->request->getPost('ket_arab'),
            'pict' => $fileName
        ]);
        $dataBerkas->move('public/admins/uploads/', $fileName);
        session()->setFlashdata('success', 'Berkas Berhasil diupload');
        return redirect()->to(base_url('productas'));
    }

    public function deletec($id)
    {
        //cari gambar berdasarkan id
        $front = $this->bahasaModel->find($id);
        //hapus gambar
        unlink('public/admins/uploads/' . $front['pict']);
        $this->bahasaModel->delete($id);
        session()->setFlashdata('success', 'Data berhasil dihapus.');

        return redirect()->to('/front');
    }

    public function mail()
    {
        $data = [
            'title' => 'Taiba Admin | Mail',
            'mail' => $this->formModel->findAll(),
        ];

        return view('admins/mail', $data);
    }

    public function article()
    {
        $data = [
            'title' => 'Taiba Admin | Article',
            'gambar' => $this->articleModel->findAll(),
        ];

        return view('admins/article', $data);
    }

    public function savearticle()
    {

        $pict = new ArticleModel();
        $pict_1 = $this->request->getFile('pict_1');
        $pict_2 = $this->request->getFile('pict_2');
        $npict_1 = $pict_1->getRandomName();
        $npict_2 = $pict_2->getRandomName();


        $pict->insert([
            'nama_article_id' => $this->request->getPost('nama_article_id'),
            'nama_article_en' => $this->request->getPost('nama_article_en'),
            'nama_article_ar' => $this->request->getPost('nama_article_ar'),
            'desc_article_id' => $this->request->getPost('desc_article_id'),
            'desc_article_en' => $this->request->getPost('desc_article_en'),
            'desc_article_ar' => $this->request->getPost('desc_article_ar'),
            'pict_1' => $npict_1,
            'pict_2' => $npict_2,
        ]);
        $pict_1->move('public/admins/uploads/', $npict_1);
        $pict_2->move('public/admins/uploads/', $npict_2);

        session()->setFlashdata('success', 'Berkas Berhasil diupload');
        return redirect()->to(base_url('/articles'));
    }
}
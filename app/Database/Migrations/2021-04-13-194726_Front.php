<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Front extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id'          => [
				'type'           => 'INT',
				'constraint'     => 11,
				'unsigned'       => true,
				'auto_increment' => true,
			],
			'pict_indo'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'pict_inggris'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'pict_arab'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'created_at' => [
				'type'           => 'DATETIME',
				'null'           => true,
			],
			'updated_at' => [
				'type'           => 'DATETIME',
				'null'           => true,
			]
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->createTable('front');
	}

	public function down()
	{
		$this->forge->dropTable('front');
	}
}

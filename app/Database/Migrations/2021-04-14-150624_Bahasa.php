<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Bahasa extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id'          => [
				'type'           => 'INT',
				'constraint'     => 11,
				'unsigned'       => true,
				'auto_increment' => true,
			],
			'category_product'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'nama_indo'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'nama_inggris'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'nama_arab'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'desc_indo'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'desc_inggris'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'desc_arab'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'ket_indo'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'ket_inggris'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'ket_arab'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'pict'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			
			'created_at' => [
				'type'           => 'DATETIME',
				'null'           => true,
			],
			'updated_at' => [
				'type'           => 'DATETIME',
				'null'           => true,
			]
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->createTable('bahasa');
	}

	public function down()
	{
		$this->forge->dropTable('bahasa');
	}
}